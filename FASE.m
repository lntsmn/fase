(* ::Package:: *)

(* ::Title:: *)
(*FeynArts Smart Environment*)


(* ::Subtitle:: *)
(*Author: Simone Lionetti*)


(* ::Section::Closed:: *)
(*Package opening*)


Needs["FeynArts`"];


(* ::Section:: *)
(*Extending Mathematica functions*)


(* ::Subsection:: *)
(*General*)


(* ::Subsubsection::Closed:: *)
(*Subsets*)


MySubsetQ::usage="MySubsetQ[A,B] returns True if A\[SubsetEqual]B and False otherwise. Repeated elements count, e.g. {a,b}\[SubsetEqual]{a,a,b} but NOT {a,a,b}\[SubsetEqual]{a,b}.";
MySubsetQ[a_List,b_List,CompareQ_:SameQ,Verbose_:False]:=
Module[
{x},
If[Verbose,Print["Called MySubsetQ[",a,",",b,"] with function ",ToString[CompareQ]]];
If[
a==={},
True,
x=Select[b,CompareQ[#,a[[1]]]&];
If[Verbose,Print[x]];
If[
x==={},
If[Verbose,Print[x],"={}"];False,
MySubsetQ[Drop[a,1],DeleteCases[b,x,1,1],CompareQ,Verbose]
]
]
];


(* ::Input:: *)
(*(*Testing...*)*)
(*Transpose[{*)
(*Tuples[{{a,b},{a,a,b},{b},{a,b,c}},2],*)
(*MySubsetQ@@@Tuples[{{a,b},{a,a,b},{b},{a,b,c}},2]*)
(*}];*)


(* ::Subsubsection::Closed:: *)
(*Proportionality*)


ProportionalQ[a_,b_]:=SameQ[a,b]||Module[{c},NumericQ[Simplify[c/.Solve[a==c b,c][[1]]]]];


(* ::Input:: *)
(*(*Testing*)*)
(*{ProportionalQ[a,a],ProportionalQ[a,b],ProportionalQ[a,2a],ProportionalQ[a-3b,-0.5a+1.5b]};*)


(* ::Subsubsection::Closed:: *)
(*Simplify complexity function*)


SimplifyCount[p_]:=Which[Head[p]===Symbol,1,IntegerQ[p],If[p==0,1,Floor[N[Log[2,Abs[p]]/Log[2,10]]]+If[p>0,1,2]],Head[p]===Rational,SimplifyCount[Numerator[p]]+SimplifyCount[Denominator[p]]+1,Head[p]===Complex,SimplifyCount[Re[p]]+SimplifyCount[Im[p]]+1,NumberQ[p],2,True,SimplifyCount[Head[p]]+If[Length[p]==0,0,Plus@@(SimplifyCount/@(List@@p))]];


(* ::Subsubsection::Closed:: *)
(*Rules*)


Swap::Usage="Swap[a,b] is equivalent to the double transformation a\[LeftRightArrow]b={a\[Rule]b,b\[Rule]a}";
Swap[a_,b_]:={a->b,b->a};


(* ::Subsection:: *)
(*Math*)


(* ::Subsubsection::Closed:: *)
(*Gamma functions*)


MakeBoxes[Gamma,StandardForm]:=MakeBoxes["\[CapitalGamma]"];


ToGammas[Arg_]:={
Gamma[n_Integer+Arg]:>(Arg+n-1)Gamma[Arg+n-1]/;Positive[n],
Gamma[n_Integer+Arg]:>Gamma[Arg+n+1]/(Arg+n)/;Negative[n](*,
Gamma[arg_]\[RuleDelayed]Gamma[Simplify[arg]]*)
};
ToGammas[Args__]:=Apply[Union,ToGammas/@{Args}];


(* ::Subsubsection:: *)
(*Hypergeometric functions*)


(* ::Subsubsubsection::Closed:: *)
(*Formatting*)


MakeBoxes[a___Hypergeometric2F1,StandardForm]:=RowBox[{RowBox[{AdjustmentBox[StyleBox["2",FontSize->Small],BoxBaselineShift->0.5],AdjustmentBox[StyleBox["F",FontFamily->"Arial",FontSize->Medium],BoxMargins->{{-0.25,-0.35},{0,0}}],AdjustmentBox[StyleBox["1\[NegativeVeryThinSpace]",FontSize->Small],BoxBaselineShift->0.5]}],"(",ToBoxes[a[[1]]],",",ToBoxes[a[[2]]],";",ToBoxes[a[[3]]],";",ToBoxes[a[[4]]],")"}]/;Length[a]==4;


(* ::Subsubsubsection::Closed:: *)
(*Special cases*)


Easy2F1[r_,b_,c_,z_]:=Module[
{n=b-c},
z^(1-c)/Pochhammer[c,n] D[z^(c+n-1)/(1-z)^r,{z,n}]
]/;IntegerQ[b-c]&&Positive[b-c];
Easy2F1[r_,b_,c_,z_]:=Module[
{n=b-c},
z^(1-c)/Pochhammer[c,n] Nest[Integrate[#,{q,0,z},GenerateConditions->False]&,q^(c+n-1)/(1-q)^r,-n]
]/;IntegerQ[b-c]&&Negative[b-c];
Easy2F1[a_,b_,c_,z_]:=Easy2F1[b,a,c,z]/;IntegerQ[c-a];
Easy2F1[a_,c_,c_,z_]:=(1-z)^-a;


(* ::Input:: *)
(*(*Cross-check*)*)
(*Table[FullSimplify[Easy2F1[a,b+n,b,z]-Hypergeometric2F1[a,b+n,b,z]],{n,0,3}]*)


(* ::Subsubsubsection::Closed:: *)
(*Argument transformations*)


Arg2F1[z->1-z]={
Hypergeometric2F1[a_,b_,c_,z_]:>Gamma[c](Gamma[c-a-b]/(Gamma[c-a]Gamma[c-b]) Hypergeometric2F1[a,b,a+b+1-c,1-z]+Gamma[a+b-c]/(Gamma[a]Gamma[b]) (1-z)^(c-a-b) Hypergeometric2F1[c-a,c-b,c+1-a-b,1-z])
};
Arg2F1[z->1/z]={
Hypergeometric2F1[a_,b_,c_,z_]:>Gamma[c](Gamma[b-a]/(Gamma[b]Gamma[c-a]) (-z)^-a Hypergeometric2F1[a,a+1-c,a+1-b,1/z]+Gamma[a-b]/(Gamma[a]Gamma[c-b]) (-z)^-b Hypergeometric2F1[b,b+1-c,b+1-a,1/z])
};
Arg2F1[z->1/(1-z)]={
Hypergeometric2F1[a_,b_,c_,z_]:>Gamma[c](Gamma[b-a]/(Gamma[b]Gamma[c-a]) (1-z)^-a Hypergeometric2F1[a,c-b,a+1-b,1/(1-z)]+Gamma[a-b]/(Gamma[a]Gamma[c-b]) (1-z)^-b Hypergeometric2F1[b,c-a,b+1-a,1/(1-z)])
};
Arg2F1[z->z/(z-1),1]={
Hypergeometric2F1[a_,b_,c_,z_]:>(1-z)^-a Hypergeometric2F1[a,c-b,c,z/(z-1)]
};
Arg2F1[z->z/(z-1),2]={
Hypergeometric2F1[a_,b_,c_,z_]:>(1-z)^-b Hypergeometric2F1[b,c-a,c,z/(z-1)]
};


Arg2F1[z->z,1]={
Hypergeometric2F1[a_,b_,c_,z_]:>(1-z)^(c-a-b) Hypergeometric2F1[c-a,c-b,c,z]
};
Arg2F1[z->z,2]={
Hypergeometric2F1[1,b_,1+b_,z_]:>1+b/(b+1) z Hypergeometric2F1[1,1+b,2+b,z]
};


(* ::Input:: *)
(*(*Check of magic identity*)*)
(*Hypergeometric2F1[1,\[Epsilon],1+\[Epsilon],z]-1-\[Epsilon]/(\[Epsilon]+1) z Hypergeometric2F1[1,1+\[Epsilon],2+\[Epsilon],z]//Series[#,{z,0,33}]&*)


(* ::Subsubsection::Closed:: *)
(*Annoying (-1+x) forms*)


To1Minus[Expr_]:=Module[{Foo},ReplaceAll[Expr,-1+z_Symbol:>-Foo(1-z)]/.Foo->1];


(* ::Section:: *)
(*Ingredients for Feynman amplitudes*)


(* ::Subsection:: *)
(*FeynArts*)


(* ::Subsubsection::Closed:: *)
(*Settings*)


$FAVerbose=0;
$CounterTerms=False;


(*Telling FeynArts there is a new object fields should be inserted in*)
P$InsertionObjects=G[_][_][__][__]|_Mass|_GaugeXi|VertexFunction[_][__]|_TensorStructure;


(* ::Subsubsection::Closed:: *)
(*Getting rid of useless crap*)


SumOver[i_Index,n_Integer,External]:=1;
SumOver[i_Index,n_Integer]:=1;(*!!!BEWARE!!!*)
ClearAll[DiracMatrix,MatrixTrace];
ClearAll[MetricTensor];
ClearAll[Process,Integral];
FermionChain[]=.;


FromFeynArtsSubs={
(*Get rid of Feynarts FourVector head, use my Vector instead*)
FourVector->Vector,
(*After having built fermion chains, forget about the Head "NonCommutative" needed by FeynArts for amplitude generation*)
FermionChain[a___,NonCommutative[b__],c___]:>FermionChain[a,b,c],
MatrixTrace[a___,NonCommutative[b__],c___]:>MatrixTrace[a,b,c],
HoldPattern[DiracSandwich[a___,NonCommutative[b__],c___]]:>DiracSandwich[a,b,c],(*New!*)
(*Replace propagators with their explicit expressions*)
FeynAmpDenominator->Times,
PropagatorDenominator[k_,m_]:>1/(SQ[k]-m^2)
};


(* ::Subsubsection::Closed:: *)
(*Converting Kronecker \[Delta]s*)


IndexDelta[Index[Colour,n_],Index[Colour,m_]]:=ColourDelta[Index[Colour,n],Index[Colour,m]];
IndexDelta[Index[Gluon,n_],Index[Gluon,m_]]:=GluonDelta[Index[Gluon,n],Index[Gluon,m]];


(* ::Subsubsection::Closed:: *)
(*Converting scalar products*)


ScalarProduct[a_,b_]:=SP[a,b];


(* ::Subsection:: *)
(*General definitions*)


(* ::Subsubsection:: *)
(*Lorentz formalism*)


(* ::Subsubsubsection::Closed:: *)
(*Lorentz vectors*)


Vector::usage="Vector[k,\[Mu]]=\!\(\*SubscriptBox[\(k\), \(\[Mu]\)]\) is the Lorentz four-vector associated with k.";
Format[Vector[k_,\[Mu]_]]:=Subscript[k, \[Mu]];
Vector/:Vector[c_?NumericQ b_,\[Mu]_]:=c Vector[b,\[Mu]];
Format[Index[Lorentz,i_Integer]]:=Subscript["\[Mu]",i];


(* ::Subsubsubsection::Closed:: *)
(*Metric tensor*)


MetricTensor::usage="MetricTensor[\[Mu],\[Nu]]=\!\(\*SubscriptBox[\(g\), \(\[Mu], \[Nu]\)]\) is the Lorentz metric.";
Format[MetricTensor[\[Mu]_,\[Nu]_]]:=Subscript["g", \[Mu],\[Nu]];
SetAttributes[MetricTensor,Orderless];
MetricTensor/:MetricTensor[\[Mu]_,\[Nu]_]Vector[p1_,\[Mu]_]:=Vector[p1,\[Nu]];
MetricTensor/:MetricTensor[\[Mu]_,\[Nu]_]MetricTensor[\[Nu]_,\[Rho]_]:=MetricTensor[\[Mu],\[Rho]];
MetricTensor/:MetricTensor[\[Mu]_,\[Nu]_]F_[crapL___,\[Nu]_,crapR___]:=F[crapL,\[Mu],crapR];(*Unfortunately, this does not always work...*)
MetricTensor/:MetricTensor[\[Mu]_,\[Mu]_]:=MyDim;
MetricTensor/:Power[MetricTensor[\[Mu]_,\[Nu]_],2]:=MyDim;


(* ::Subsubsubsection::Closed:: *)
(*Scalar products*)


SP::usage="SP[\!\(\*SubscriptBox[\(p\), \(1\)]\),\!\(\*SubscriptBox[\(p\), \(2\)]\)] is the (Lorentz) scalar product of the vectors \!\(\*SubscriptBox[\(p\), \(1\)]\) and \!\(\*SubscriptBox[\(p\), \(2\)]\).";
Format[SP[p1_,p2_]]:=p1.p2;
SetAttributes[SP,Orderless];
Vector/:Vector[p1_,\[Mu]_]Vector[p2_,\[Mu]_]:=SP[p1,p2];
SP[a_,b_+c_]:=SP[a,b]+SP[a,c];
SP[a_?NumericQ b_,c_]:=a SP[b,c];
SP[0,a_]:=0;
SQ::usage="SQ[p] is a shorthand for SP[p,p].";
SQ[p1_]:=SP[p1,p1];
Vector/:Power[Vector[p_,\[Mu]_],2]:=SQ[p];


(* ::Subsubsection:: *)
(*Amplitude building blocks*)


(* ::Subsubsubsection::Closed:: *)
(*Four-momenta*)


Format[FourMomentum[External,n_Integer]]:=Subscript["p",n];
Format[FourMomentum[Incoming,n_Integer]]:=Subscript["p",n];
Format[FourMomentum[Outgoing,n_Integer]]:=Subsuperscript["p",n,","];
Format[FourMomentum[Internal,n_Integer]]:=Subscript["k",n];


(* ::Subsubsubsection::Closed:: *)
(*Fermion chains*)


Format[FermionChain[DiracSpinor[FourMomentum[External,i1_Integer],m1_],r___,DiracSpinor[FourMomentum[External,i2_Integer],m2_]]]:=FermionChain[Overscript["u","_"][FourMomentum[External,i1],m1],r,"u"[FourMomentum[External,i2],m2]];
Format[FermionChain[DiracSpinor[FourMomentum[External,i1_Integer],m1_],r___,DiracSpinor[-FourMomentum[External,i2_Integer],m2_]]]:=FermionChain[Overscript["u","_"][FourMomentum[External,i1],m1],r,"v"[FourMomentum[External,i2],m2]];
Format[FermionChain[DiracSpinor[-FourMomentum[External,i1_Integer],m1_],r___,DiracSpinor[FourMomentum[External,i2_Integer],m2_]]]:=FermionChain[Overscript["v","_"][FourMomentum[External,i1],m1],r,"u"[FourMomentum[External,i2],m2]];
Format[FermionChain[DiracSpinor[-FourMomentum[External,i1_Integer],m1_],r___,DiracSpinor[-FourMomentum[External,i2_Integer],m2_]]]:=FermionChain[Overscript["v","_"][FourMomentum[External,i1],m1],r,"v"[FourMomentum[External,i2],m2]];


(* ::Subsubsubsection::Closed:: *)
(*Polarization vectors*)


MakeBoxes[PolarizationVector[V[crap__],k_,\[Mu]_Index],StandardForm]:=RowBox[{SubscriptBox["\[Epsilon]",MakeBoxes[\[Mu],StandardForm]],"[",MakeBoxes[k,StandardForm],"]"}];
MakeBoxes[Conjugate[PolarizationVector][V[crap__],k_,\[Mu]_Index],StandardForm]:=RowBox[{SubsuperscriptBox["\[Epsilon]",MakeBoxes[\[Mu],StandardForm],"*"],"[",MakeBoxes[k,StandardForm],"]"}];


(* ::Subsubsubsection::Closed:: *)
(*Tensor structures*)


TensorStructure::usage="TensorStructure[Particle,Type][\[Mu],\[Nu]][k] is the tensor structure associated the vector boson Particle of momentum k.
Type specifies whether the structure is associated to an Internal or External particle and allows for mixed gauge choices.";
(*Formatting*)
MakeBoxes[TensorStructure[Particle_,Type_][\[Mu]_,\[Nu]_][k_],StandardForm]:=RowBox[{SubsuperscriptBox["\[CapitalPi]",MakeBoxes[Particle],RowBox[{MakeBoxes[\[Mu]],MakeBoxes[\[Nu]]}]],"[",MakeBoxes[k],"]"}];


(* ::Subsubsubsection::Closed:: *)
(*Slash notation*)


Format[DiracSlash[FourMomentum[Type_,i_Integer]]]:=Subscript[Switch[Type,External,"p",Internal,"k",Incoming,"p",Outgoing,"\!\(\*SuperscriptBox[\(p\), \(,\)]\)"]<>"\[NegativeThickSpace]\!\(\*
StyleBox[\"/\",\nFontSize->16]\)\!\(\*
StyleBox[\"\[ThinSpace]\",\nFontSize->16]\)","\[NegativeThinSpace]"<>ToString[i]];
Format[DiracSlash[Eta[i_]]]:=Subscript["\[Eta]"<>"\[NegativeThickSpace]/\[VeryThinSpace]\[VeryThinSpace]",i];
Format[DiracSlash[y__]]:=ReplaceRepeated[DiracSlash[y],Join[DiracSlashCollect,DiracSlashExpand]]/;And[FreeQ[y,Pattern],Head[y]==Plus||Head[y]==Times];


(* ::Subsubsubsection::Closed:: *)
(*Non-commutative objects*)


NonCommutativeObjects={DiracSpinor,DiracSlash,DiracMatrix,ChiralityProjector};
CommutativeQ[a_]:=And@@Map[FreeQ[a,#]&,NonCommutativeObjects];
NonCommutativeQ[a_]:=!CommutativeQ[a];


(* ::Subsubsection:: *)
(*Common substitutions and properties*)


(* ::Subsubsubsection::Closed:: *)
(*Lorentz vectors*)


VectorExpand={
Vector[a_+b_,\[Mu]_]:>Vector[a,\[Mu]]+Vector[b,\[Mu]],
Vector[a_-b_,\[Mu]_]:>Vector[a,\[Mu]]-Vector[b,\[Mu]]
};
VectorCollect={
Vector[a_?NumericQ b_]:>a Vector[b]
};
VectorMerge={
Vector[a_,\[Mu]_]+Vector[b_,\[Mu]_]:>Vector[a+b,\[Mu]],
Vector[a_,\[Mu]_]-Vector[b_,\[Mu]_]:>Vector[a-b,\[Mu]]
};


(* ::Subsubsubsection::Closed:: *)
(*Contractions*)


Contractions={
(*Vector[p1_,\[Mu]_]Vector[p2_,\[Mu]_]\[RuleDelayed]SP[p1,p2],*)
MetricTensor[\[Mu]_,\[Nu]_]Vector[p1_,\[Mu]_]:>Vector[p1,\[Nu]],
MetricTensor[\[Mu]_,\[Nu]_] FermionChain[a___,b_[crapL___,\[Mu]_,crapR___],c___]:>FermionChain[a,b[crapL,\[Nu],crapR],c],
Vector[k_,\[Mu]_] FermionChain[a___,DiracMatrix[\[Mu]_],c___]:>FermionChain[a,DiracSlash[k],c],
MetricTensor[\[Mu]_,\[Nu]_] MatrixTrace[a___,b_[crapL___,\[Mu]_,crapR___],c___]:>MatrixTrace[a,b[crapL,\[Nu],crapR],c],
Vector[k_,\[Mu]_] MatrixTrace[a___,DiracMatrix[\[Mu]_],c___]:>MatrixTrace[a,DiracSlash[k],c],
MetricTensor[\[Mu]_,\[Nu]_] NonCommutative[a___,b_[crapL___,\[Mu]_,crapR___],c___]:>NonCommutative[a,b[crapL,\[Nu],crapR],c],
Vector[k_,\[Mu]_]NonCommutative[a___,DiracMatrix[\[Mu]_],b___]:>NonCommutative[a,DiracSlash[k],b]
};


(* ::Subsubsubsection::Closed:: *)
(*Fermion chains*)


FermionChainExpand={
FermionChain[a___,b_+c_,d___]:>FermionChain[a,b,d]+FermionChain[a,c,d],
FermionChain[a___,b_-c_,d___]:>FermionChain[a,b,d]-FermionChain[a,c,d]
};
FermionChainCollect={
FermionChain[a___,b_?CommutativeQ,c___]:>b FermionChain[a,c],
FermionChain[a___,b_?CommutativeQ d_,c___]:>b FermionChain[a,d,c]
};
FermionChainMerge={
FermionChain[a___,b_,d___]+FermionChain[a___,c_,d___]:>FermionChain[a,b+c,d],
FermionChain[a___,b_,d___]-FermionChain[a___,c_,d___]:>FermionChain[a,b-c,d]
};


(* ::Subsubsubsection::Closed:: *)
(*Matrix traces*)


MatrixTraceExpand={
MatrixTrace[a___,b_+c_,d___]:>MatrixTrace[a,b,d]+MatrixTrace[a,c,d],
MatrixTrace[a___,b_-c_,d___]:>MatrixTrace[a,b,d]-MatrixTrace[a,c,d]
};
MatrixTraceCollect={
MatrixTrace[a___,b_?CommutativeQ,c___]:>b MatrixTrace[a,c],
MatrixTrace[a___,b_?CommutativeQ d_,c___]:>b MatrixTrace[a,d,c],
MatrixTrace[]->4
};
MatrixTraceMerge={
MatrixTrace[a___,b_,d___]+MatrixTrace[a___,c_,d___]:>MatrixTrace[a,b+c,d],
MatrixTrace[a___,b_,d___]-MatrixTrace[a___,c_,d___]:>MatrixTrace[a,b-c,d]
};


(* ::Subsubsubsection::Closed:: *)
(*Dirac slashes*)


DiracSlashExpand={
DiracSlash[a_+b_]:>DiracSlash[a]+DiracSlash[b],
DiracSlash[a_-b_]:>DiracSlash[a]-DiracSlash[b]
};
DiracSlashCollect={
DiracSlash[a_Plus]:>-DiracSlash[-a]/;Apply[And,MatchQ[#,b_?Negative c_]&/@List@@a],
DiracSlash[a_?NumericQ b_]:> a DiracSlash[b]
};
DiracSlashMerge={
HoldPattern[Plus[DiracSlash[a__],c_?NumericQ DiracSlash[b_]]]:>DiracSlash[a+c b],
HoldPattern[Plus[a__DiracSlash]]:>DiracSlash[Apply[Plus,#[[1]]&/@{a}]]
};


(* ::Subsubsubsection::Closed:: *)
(*Dirac equation*)


DiracEquationSubs={
(*Slashed momentum just next to the spinor*)
FermionChain[crapL__,DiracSlash[FourMomentum[External,i_Integer]],DiracSpinor[FourMomentum[External,i_],m_]]:>m FermionChain[crapL,DiracSpinor[FourMomentum[External,i],m]],
FermionChain[crapL__,DiracSlash[FourMomentum[External,i_Integer]],DiracSpinor[-FourMomentum[External,i_],m_]]:>-m FermionChain[crapL,DiracSpinor[-FourMomentum[External,i],m]],
FermionChain[DiracSpinor[FourMomentum[External,i_],m_],DiracSlash[FourMomentum[External,i_Integer]],crapR__]:>m FermionChain[DiracSpinor[FourMomentum[External,i],m],crapR],
FermionChain[DiracSpinor[-FourMomentum[External,i_],m_],DiracSlash[FourMomentum[External,i_Integer]],crapR__]:>-m FermionChain[DiracSpinor[-FourMomentum[External,i],m],crapR],
(*Moving closer a slashed momentum which is not next to the spinor*)
(*1. through another slash*)
FermionChain[crapL__,DiracSlash[k_],DiracSlash[q_],crapM___,DiracSpinor[k_,m_]]:>2SP[k,q]FermionChain[crapL,crapM,DiracSpinor[k,m]]-FermionChain[crapL,DiracSlash[q],DiracSlash[k],crapM,DiracSpinor[k,m]],
FermionChain[crapL__,DiracSlash[k_],DiracSlash[q_],crapM___,DiracSpinor[-k_,m_]]:>2SP[k,q]FermionChain[crapL,crapM,DiracSpinor[-k,m]]-FermionChain[crapL,DiracSlash[q],DiracSlash[k],crapM,DiracSpinor[-k,m]],
FermionChain[DiracSpinor[k_,m_],crapM___,DiracSlash[q_],DiracSlash[k_],crapR__]:>2SP[k,q]FermionChain[DiracSpinor[k,m],crapM,crapR]-FermionChain[DiracSpinor[k,m],crapM,DiracSlash[k],DiracSlash[q],crapR],
FermionChain[DiracSpinor[-k_,m_],crapM___,DiracSlash[q_],DiracSlash[k_],crapR__]:>2SP[k,q]FermionChain[DiracSpinor[-k,m],crapM,crapR]-FermionChain[DiracSpinor[-k,m],crapM,DiracSlash[k],DiracSlash[q],crapR],
(*2. through a \[Gamma] matrix*)
FermionChain[crapL__,DiracSlash[k_],DiracMatrix[\[Mu]_],crapM___,DiracSpinor[k_,m_]]:>2Vector[k,\[Mu]]FermionChain[crapL,crapM,DiracSpinor[k,m]]-FermionChain[crapL,DiracMatrix[\[Mu]],DiracSlash[k],crapM,DiracSpinor[k,m]],
FermionChain[crapL__,DiracSlash[k_],DiracMatrix[\[Mu]_],crapM___,DiracSpinor[-k_,m_]]:>2Vector[k,\[Mu]]FermionChain[crapL,crapM,DiracSpinor[-k,m]]-FermionChain[crapL,DiracMatrix[\[Mu]],DiracSlash[k],crapM,DiracSpinor[-k,m]],
FermionChain[DiracSpinor[k_,m_],crapM___,DiracMatrix[\[Mu]_],DiracSlash[k_],crapR__]:>2Vector[k,\[Mu]]FermionChain[DiracSpinor[k,m],crapM,crapR]-FermionChain[DiracSpinor[k,m],crapM,DiracSlash[k],DiracMatrix[\[Mu]],crapR],
FermionChain[DiracSpinor[-k_,m_],crapM___,DiracMatrix[\[Mu]_],DiracSlash[k_],crapR__]:>2Vector[k,\[Mu]]FermionChain[DiracSpinor[-k,m],crapM,crapR]-FermionChain[DiracSpinor[-k,m],crapM,DiracSlash[k],DiracMatrix[\[Mu]],crapR]
};


(* ::Subsubsubsection::Closed:: *)
(*Double slashes*)


DoubleSlashSubs={
(*1. Fermion chains *)
(*1.1 Chains with kk *)
FermionChain[crapL___,DiracSlash[k_],DiracSlash[k_],crapR___]:>SQ[k]FermionChain[crapL,crapR],
(*1.2 Chains with k...k - anticommuting to get ks closer *)
FermionChain[crapL___,DiracSlash[k_],DiracSlash[q_],crapM___,DiracSlash[k_],crapR___]:>2SP[k,q]FermionChain[crapL,crapM,DiracSlash[k],crapR]-FermionChain[crapL,DiracSlash[q],DiracSlash[k],crapM,DiracSlash[k],crapR]/;Length[DeleteDuplicates[{DiracSlash[q],crapM}]]==Length[{DiracSlash[q],crapM}],
FermionChain[crapL___,DiracSlash[k_],DiracMatrix[\[Mu]_],crapM___,DiracSlash[k_],crapR___]:>2Vector[k,\[Mu]]FermionChain[crapL,crapM,DiracSlash[k],crapR]-FermionChain[crapL,DiracMatrix[\[Mu]],DiracSlash[k],crapM,DiracSlash[k],crapR]/;Length[DeleteDuplicates[{DiracMatrix[\[Mu]],crapM}]]==Length[{DiracMatrix[\[Mu]],crapM}],
(*2. Matrix traces *)
(*2.1 Tr[...kk...] and Tr[k...k] *)
MatrixTrace[crapL___,DiracSlash[k_],DiracSlash[k_],crapR___]:>SQ[k]MatrixTrace[crapL,crapR],
MatrixTrace[DiracSlash[k_],crap__,DiracSlash[k_]]:>SQ[k]MatrixTrace[crap],
(*2.2 Tr[k..k....] - anticommuting to get ks closer *)
MatrixTrace[DiracSlash[k_],crapM___,DiracSlash[q_],DiracSlash[k_],crapR__]:>2SP[k,q]MatrixTrace[DiracSlash[k],crapM,crapR]-MatrixTrace[DiracSlash[k],crapM,DiracSlash[k],DiracSlash[q],crapR]/;Length[{crapM}]+1<=Length[{crapR}],
MatrixTrace[DiracSlash[k_],crapM___,DiracMatrix[\[Mu]_],DiracSlash[k_],crapR__]:>2Vector[k,\[Mu]]MatrixTrace[DiracSlash[k],crapM,crapR]-MatrixTrace[DiracSlash[k],crapM,DiracSlash[k],DiracMatrix[\[Mu]],crapR]/;Length[{crapM}]+1<=Length[{crapR}],
(*2.3 Tr[...k...k...] and Tr[k....k..] - casting into Tr[k..k....] by cycling *)
MatrixTrace[DiracSlash[k_],crapM__,DiracSlash[k_],crapR__]:>MatrixTrace[DiracSlash[k],crapR,DiracSlash[k],crapM]/;Length[{crapM}]>Length[{crapR}]&&Not[MemberQ[{crapM},DiracSlash[k]]],
MatrixTrace[DiracSlash[k_],crapM__,DiracSlash[k_],crapR___]:>MatrixTrace[DiracSlash[k],crapM,DiracSlash[k],crapR],
MatrixTrace[crapL__,DiracSlash[k_],crapM___,DiracSlash[k_],crapR___]:>MatrixTrace[DiracSlash[k],crapM,DiracSlash[k],crapR,crapL]/;Not[MemberQ[{crapL,crapM},DiracSlash[k]]]
};
(* !!! TEST 2.2 and 2.3 !!! *)


(* ::Subsubsubsection::Closed:: *)
(*Chirality projectors*)


ChiralityProjector/:ChiralityProjector[1]+ChiralityProjector[-1]:=1;
KillChiralityProjectors={
ChiralityProjector[1]->1,
ChiralityProjector[-1]->0
};


(* ::Subsubsubsection::Closed:: *)
(*Amputating amplitudes*)


Amputate[Amplitude_]:=Amplitude/.{
FermionChain[DiracSpinor[__],a__,DiracSpinor[__]]:>NonCommutative[a],
FermionChain[DiracSpinor[__],DiracSpinor[__]]:>1,
Conjugate[PolarizationVector][___]:>1,
PolarizationVector[___]:>1
}


(* ::Subsubsection:: *)
(*SU(N) algebra*)


(* ::Subsubsubsection::Closed:: *)
(*N and Casimirs*)


Format[ColourN]:="\!\(\*SubscriptBox[\(N\), \(c\)]\)";
Format[SUNCF]:="\!\(\*SubscriptBox[\(C\), \(F\)]\)";
Format[SUNCA]:="\!\(\*SubscriptBox[\(C\), \(A\)]\)";
CACF:={SUNCF->(ColourN^2-1)/(2ColourN),SUNCA->ColourN};


(* ::Subsubsubsection::Closed:: *)
(*Fundamental representation*)


(*Colour \[Delta] and properties*)
Format[ColourDelta[i_Index,j_Index]]:=Subscript[\[Delta], i,j];
SetAttributes[ColourDelta,Orderless];
ColourDelta[i_,i_]:=ColourN;
ColourDelta/:Power[ColourDelta[i_,j_],2]:=ColourN;
ColourDelta/:Conjugate[ColourDelta[a__Index]]:=ColourDelta[a];
ColourDelta/:ColourDelta[i_Index,j_Index]f_[l___Index,i_Index,m___Index]:=f[l,j,m];


(*Colour T matrices and properties*)
Format[SUNT[a_,i_,j_]]:=













\!\(\*SubsuperscriptBox[\(T\), \(i, j\), \(a\)]\);
SUNT/:Conjugate[SUNT[a_,i_,j_]]:=SUNT[a,j,i];
SUNT/:SUNT[a_,i_,j_]SUNT[a_,j_,k_]:=SUNCF ColourDelta[i,k];
SUNT/:SUNT[a_,i_,j_]SUNT[b_,j_,i_]:=1/2 GluonDelta[a,b];
SUNT/:SUNT[a_,i_,j_]SUNT[b_,j_,k_]SUNT[a_,k_,l_]:=(SUNCF-1/2 SUNCA)SUNT[b,i,l];


(* ::Subsubsubsection::Closed:: *)
(*Adjoint representation*)


(*Adjoint \[Delta] and properties*)
Format[GluonDelta[i_Index,j_Index]]:=Subscript[\[Delta], i,j];
SetAttributes[GluonDelta,Orderless];
GluonDelta[i_,i_]:=ColourN^2-1;
GluonDelta/:Conjugate[GluonDelta[a__Index]]:=GluonDelta[a];
GluonDelta/:GluonDelta[i_Index,j_Index]f_[l___Index,i_Index,m___Index]:=f[l,j,m];


(*Adjoint f matrices and properties*)
Format[SUNF[a_,b_,c_]]:=("f")^ToString[a]<>","<>ToString[b]<>","<>ToString[c];
SUNF/:Conjugate[SUNF[a__]]:=SUNF[a];
SUNF/:SUNF[a_,b_,c_]SUNT[b_,i_,j_]SUNT[c_,j_,k_]:=I/2 SUNCA SUNT[a,i,k];
SUNF/:SUNF[a_,c_,b_]SUNT[b_,i_,j_]SUNT[c_,j_,k_]:=-(I/2)SUNCA SUNT[a,i,k];
SUNF/:SUNF[a_,c_,d_]SUNF[b_,c_,d_]:=SUNCA*GluonDelta[a,b];
SUNF/:Power[SUNF[a_,b_,c_],2]:=Module[{d},GluonDelta[c,d]SUNF[a,b,c]*SUNF[a,b,d]];


(* ::Subsubsection:: *)
(*Conjugation*)


(* ::Subsubsubsection::Closed:: *)
(*MyAmplConj command*)


MyAmplConj[Amplitude_]:=
ReplaceRepeated[
ReplaceAll[
Conjugate[Amplitude],
{
Index[Type_,N_]:>Index[Type,-N]
}
],
{
(*Hacking FeynArts syntax for polarization vectors*)
Conjugate[Conjugate[PolarizationVector][crap__]]:> PolarizationVector[crap],
Conjugate[Plus[a_,b_]]:>Conjugate[a]+Conjugate[b],
Conjugate[Times[a_,b_]]:>Conjugate[a]Conjugate[b]
}
];
Format[MyAmplConj]:=Conjugate;


(* ::Subsubsubsection::Closed:: *)
(*Momenta*)


Vector/:Conjugate[Vector[FourMomentum[Type_,Integer_],\[Mu]_]]:=Vector[FourMomentum[Type,Integer],\[Mu]];
SP/:Conjugate[SP[FourMomentum[Type1_,i1_Integer],FourMomentum[Type2_,i2_Integer]]]:=SP[FourMomentum[Type1,i1],FourMomentum[Type2,i2]];
MetricTensor/:Conjugate[MetricTensor[a___]]:=MetricTensor[a];


(* ::Subsubsubsection::Closed:: *)
(*Fermion chains*)


MyDim/:Conjugate[MyDim]:=MyDim;
FermionChain/:Conjugate[FermionChain[a__]]:=Reverse[FermionChain[a]];


(* ::Subsubsubsection::Closed:: *)
(*SU(N) elements*)


SUNCA/:Conjugate[SUNCA]:=SUNCA;
SUNCF/:Conjugate[SUNCF]:=SUNCF;


(* ::Subsubsubsection::Closed:: *)
(*Couplings and masses*)


(***** Standard Model *****)
(*Couplings*)
EL/:Conjugate[EL]=EL;
GS/:Conjugate[GS]=GS;
YB/:Conjugate[YB]=YB;
(*Masses*)
MH/:Conjugate[MH]=MH;
MW/:Conjugate[MW]=MW;
SW/:Conjugate[SW]=SW;
MB/:Conjugate[MB]=MB;


(***** Hierarchy Yukawa theory *****)
(*Couplings*)
Yq/:Conjugate[Yq]=Yq;
YQ/:Conjugate[YQ]=YQ;
(*Masses*)
Mq/:Conjugate[Mq]=Mq;
MQ/:Conjugate[MQ]=MQ;


(* ::Subsubsection:: *)
(*Polarization and spin sums*)


(* ::Subsubsubsection::Closed:: *)
(*Polarization sums*)


PolarizationSum[Amplitude_]:=
ReplaceRepeated[
Amplitude,
{
PolarizationVector[Particle1_,k_,\[Mu]_]Conjugate[PolarizationVector][Particle2_,k_,\[Nu]_]:>-TensorStructure[Particle1,External][\[Mu],\[Nu]][k]/;MyAmplConj[Particle1]==Conjugate[Particle2]
}
];


PolarizationSum[Amplitude_,Particle_]:=
ReplaceRepeated[
Amplitude,
{
PolarizationVector[Particle1_,k_,\[Mu]_]Conjugate[PolarizationVector][Particle2_,k_,\[Nu]_]:>-TensorStructure[Particle1,External][\[Mu],\[Nu]][k]/;MyAmplConj[Particle1]==Conjugate[Particle2]&&(Particle1==Particle||Particle2==Particle)
}
];


(* ::Subsubsubsection::Closed:: *)
(*Spin sums*)


SpinSum[Amplitude_]:=
ReplaceRepeated[
Amplitude,
{
(*Connecting two chains*)
FermionChain[a__,DiracSpinor[FourMomentum[External,i_Integer],m_]]FermionChain[DiracSpinor[FourMomentum[External,i_Integer],m_],b__]:>FermionChain[a,DiracSlash[FourMomentum[External,i]]+m,b],
FermionChain[a__,DiracSpinor[-FourMomentum[External,i_Integer],m_]]FermionChain[DiracSpinor[-FourMomentum[External,i_Integer],m_],b__]:>FermionChain[a,DiracSlash[FourMomentum[External,i]]-m,b],
(*Closing a chain*)
FermionChain[DiracSpinor[FourMomentum[External,i_Integer],m_],crap__,DiracSpinor[FourMomentum[External,i_Integer],m_]]:> MatrixTrace[DiracSlash[FourMomentum[External,i]]+m,crap],
FermionChain[DiracSpinor[-FourMomentum[External,i_Integer],m_],crap__,DiracSpinor[-FourMomentum[External,i_Integer],m_]]:> MatrixTrace[DiracSlash[FourMomentum[External,i]]-m,crap]
}
];


(* ::Subsubsection:: *)
(*Gauge choice*)


(* ::Subsubsubsection::Closed:: *)
(*Subscript[R, \[Xi]] gauge*)


Covariant::usage="Covariant is the flag used to denote covariant (\!\(\*SubscriptBox[\(R\), \(\[Xi]\)]\)) gauge.";
Protect[Covariant];


GaugeXi::usage="GaugeXi is the Head used to denote the constant which corresponds to a covariant gauge choice.";
GaugeXiBar::usage="GaugeXiBar is a shorthand for 1-GaugeXi.";
Format[GaugeXi[Particle_]]:=Subscript["\[Xi]",Particle];
Format[GaugeXiBar[Particle_]]:=(1-GaugeXi[Particle]);


TensorStructure[Covariant,\[Xi]bar_][\[Mu]_,\[Nu]_][k_]:=-MetricTensor[\[Mu],\[Nu]]+\[Xi]bar (Vector[k,\[Mu]]Vector[k,\[Nu]])/SP[k,k];


TensorStructure[Covariant,Particle_,Type_][\[Mu]_,\[Nu]_][k_]:=TensorStructure[Covariant,GaugeXiBar[Particle]][\[Mu],\[Nu]][k];


(* ::Subsubsubsection::Closed:: *)
(*Feynman gauge*)


Feynman::usage="Feynman is the flag used to denote Feynman gauge.";
Protect[Feynman];


TensorStructure[Feynman,Particle_,Type_][\[Mu]_,\[Nu]_][k_]:=-MetricTensor[\[Mu],\[Nu]];


(* ::Subsubsubsection::Closed:: *)
(*Axial gauge*)


(* ::Text:: *)
(*Remember \[Eta](k)\[CenterDot]k!=0, \[Eta](k)\[CenterDot]\[Epsilon](k)=0*)


Axial::usage="Axial is the flag used to denote axial gauge.";
Axial::type="Axial gauge is defined only for types External or Internal.";
Protect[Axial];


Eta::usage="Eta is the Head used for axial gauge vectors.";
Format[Eta[Particle_]]:=Subscript["\[Eta]",Particle];


TensorStructure[Axial,\[Eta]_][\[Mu]_,\[Nu]_][k_]:=-MetricTensor[\[Mu],\[Nu]]+(Vector[\[Eta],\[Mu]]Vector[k,\[Nu]]+Vector[k,\[Mu]]Vector[\[Eta],\[Nu]])/SP[\[Eta],k]-(SP[\[Eta],\[Eta]]Vector[k,\[Mu]]Vector[k,\[Nu]])/(SP[\[Eta],k])^2;


TensorStructure[Axial,Particle_,Type_][\[Mu]_,\[Nu]_][k_]:=
Switch[Type,
Internal,TensorStructure[Axial,Eta[Particle]][\[Mu],\[Nu]][k],
External,TensorStructure[Axial,Eta[Particle][k]][\[Mu],\[Nu]][k],
_,Message[Axial::type]
];


(* ::Subsubsubsection::Closed:: *)
(*Fixing a gauge*)


ClearAll[FixGauge];
FixGauge::usage="FixGauge[{ParticleType,Internal/External}->Gauge] fixes the gauge in Internal/External propagators
to Gauge for particles of type ParticleType (e.g. V[5,_] for gluons)";
FixGauge[Rules__Rule][Expr_]:=Expr/.FixGauge[Rules];
FixGauge[MyRule_Rule]:=
RuleDelayed@@@Switch[
Length[MyRule[[1]]],
1,{{TensorStructure[MyRule[[1]],Internal],TensorStructure[MyRule[[2]],MyRule[[1]],Internal]},{TensorStructure[MyRule[[1]],External],TensorStructure[MyRule[[2]],MyRule[[1]],External]}},
2,{{TensorStructure[MyRule[[1]][[1]],MyRule[[1]][[2]]],TensorStructure[MyRule[[2]],MyRule[[1]][[1]],MyRule[[1]][[2]]]/.x_Pattern:>x[[1]]}},
_,Message[FixGauge::usage];{{}}
];
FixGauge[Rules__Rule]:=Apply[Join,FixGauge/@{Rules}];


(* ::Subsubsection:: *)
(*Dirac algebra*)


(* ::Subsubsubsection::Closed:: *)
(*Output formatting*)


Format[MatrixTrace[a__]]:=Tr[Dot[a]];
Format[DiracMatrix[\[Mu]_]]:=\[Gamma]^\[Mu];


(* ::Subsubsubsection::Closed:: *)
(*Reducing traces*)


DoTraces[Expr_]:=Expr/.{a_MatrixTrace:>DoTraces`DoTrace[a]};


DoTraces`DoTrace[Expr_]:=ReplaceRepeated[
Expr//.DiracSlashMerge,
MatrixTrace[a___,DiracSlash[p_],b___]:>
Module[
{\[Mu]},
Vector[p,\[Mu]]MatrixTrace[a,DiracMatrix[\[Mu]],b]
]
]//FeynExpand//Simplify;


(* ::Subsubsubsection::Closed:: *)
(*Performing traces of \[Gamma] matrices*)


MatrixTrace[a___]:=0/;And[And@@Map[MatchQ[#,_DiracSlash|_DiracMatrix]&,{a}],OddQ[Length[{a}]]];
MatrixTrace[\[Gamma]s__DiracMatrix]:=Apply[MyMatrixTrace,#[[1]]&/@{\[Gamma]s}]/;EvenQ[Length[{\[Gamma]s}]];
MyMatrixTrace[N_Integer]:=
MyMatrixTrace[N]=
Expand[
Plus@@
Table[
(-1)^i MetricTensor[MyMu[1], MyMu[i]] Apply[MyMatrixTrace,MyMu/@Drop[Range[2,N], {i-1}]],
{i,2,N}
]
];
MyMatrixTrace[mus__]:=
ReplaceAll[
MyMatrixTrace[Length[{mus}]],
Rule@@#&/@Transpose[{MyMu/@Range[Length[{mus}]],{mus}}]
]/;Apply[
And,
Head[#]=!=Integer&/@{mus}
];
MyMatrixTrace[]=4;


(* ::Input:: *)
(*(*Check number of terms in a matrix trace of 2n matrices*)*)
(*Module[*)
(*{n=5},*)
(*Apply[MatrixTrace,DiracMatrix[Index[Lorentz,#]]&/@Range[2n]]//Length[#]==(2n-1)!!&*)
(*]*)


(* ::Subsubsubsection::Closed:: *)
(*Dirac Sandwiches*)


ClearAll[DiracSandwich];
DiracSandwich[a___,b_?CommutativeQ,c___]:=b DiracSandwich[a,c];
DiracSandwich[a___,b_?CommutativeQ d_,c___]:=b DiracSandwich[a,d,c];
DiracSandwich[crapL___,DiracSlash[k_],crapR___]:=Module[{\[Mu]},ReplaceAll[Expand[Vector[k,\[Mu]]DiracSandwich[crapL,DiracMatrix[\[Mu]],crapR],NonCommutative],Contractions]];
DiracSandwich[crapL___,k_Plus,crapR___]:=
DiracSandwich[crapL,k/.DiracSlashMerge,crapR]/;MatchQ[k,Apply[Alternatives,#[[1]]&/@DiracSlashMerge]];
DiracSandwich[crapL___,k_Plus,crapR___]:=DiracSandwich[crapL,#,crapR]&/@k;
DiracSandwich[]=MyDim;
DiracSandwich[a_DiracMatrix]:=(2-MyDim)NonCommutative[a];
DiracSandwich[a__DiracMatrix]:=
Collect[
Module[
{
Indeces=Level[{a},{2}],
n=Length[{a}]
},
4Sum[
(-1)^(i+1) MetricTensor[Indeces[[n]],Indeces[[i]]]Apply[NonCommutative,DiracMatrix/@Drop[Most[Indeces],{i}]],
{i,1,n-1}
]
+2(-1)^(n+1) Apply[NonCommutative,{a}]
-Module[
{Foo=ReplaceAll[DiracSandwich@@Most[{a}],NonCommutative[b___]:>NonCommutative[b,Last[{a}]]]},
If[
Head[Foo]=!=Plus||!FreeQ[#,NonCommutative],
#,
# NonCommutative[Last[{a}]]
]&/@Foo
]
]/.NonCommutative[]->1,
NonCommutative[___],
Simplify
];


(* ::Input:: *)
(*(*Check Dirac Sandwich evaluation against standard trace*)*)
(*(*Done up to n=5*)*)
(*Module[*)
(*{*)
(*n=4,*)
(*MyTrace,*)
(*Sandwich,*)
(*Standard*)
(*},*)
(*MyTrace=Vector[FourMomentum[External,1],Index[Lorentz,n+2]]Apply[FermionChain,DiracMatrix[Index[Lorentz,#]]&/@Range[2n]]/.{n+1->1}/.Contractions;*)
(*MyTrace=MyTrace/.FermionChain->MatrixTrace;*)
(*Print[MyTrace];*)
(*Sandwich=Timing[DoTraces[FeynExpandRepeated[MyTrace]]];*)
(*Print[Sandwich[[1]]];*)
(*Standard=Timing[DoTraces[MyTrace]];*)
(*Print[Standard[[1]]];*)
(*Expand[Sandwich[[2]]-Standard[[2]]]==0*)
(*]*)


(* ::Subsubsection:: *)
(*Extending Simplify and Expand*)


(* ::Subsubsubsection::Closed:: *)
(*Transformations*)


(*Try putting together non-commutative chains*)
FeynTransformation[1][Expr_]:=Expr/.{NonCommutative[a___,b_,c___]+NonCommutative[a___,d_,c___]:>NonCommutative[a,b+d,c]};
(*Try separating and putting together vectors, fermion chains and Dirac slashes*)
FeynTransformation[2][Expr_]:=Expr/.VectorExpand;
FeynTransformation[3][Expr_]:=Expr/.FermionChainExpand;
FeynTransformation[4][Expr_]:=Expr/.DiracSlashExpand;
FeynTransformation[5][Expr_]:=Expr/.MatrixTraceExpand;
FeynTransformation[6][Expr_]:=Expr/.VectorMerge;
FeynTransformation[7][Expr_]:=Expr/.FermionChainMerge;
FeynTransformation[8][Expr_]:=Expr/.DiracSlashMerge;
FeynTransformation[9][Expr_]:=Expr/.MatrixTraceMerge;
(*Total number of transformations - please change*)
FeynTransformationN=9;


(* ::Subsubsubsection::Closed:: *)
(*Substitutions*)


(*Always convert FeynArts' notation to his notebook's*)
FeynSubstitute`Rule[1]=FromFeynArtsSubs;
(*Always try to pull out factors from vectors, fermion chains and Dirac slashes*)
FeynSubstitute`Rule[2]=FermionChainCollect;
FeynSubstitute`Rule[3]=VectorCollect;
FeynSubstitute`Rule[4]=DiracSlashCollect;
FeynSubstitute`Rule[5]=MatrixTraceCollect;
(*Always write contracted indices as scalar products*)
FeynSubstitute`Rule[6]=Contractions;
(*Always write double Dirac slashes as squares*)
FeynSubstitute`Rule[7]=DoubleSlashSubs;
(*Try simplifying fermion chains using Dirac's equation*)
FeynSubstitute`Rule[8]=DiracEquationSubs;
(*Always try to recognize and work out Dirac sandwiches*)
FeynSubstitute`Rule[9]={FermionChain[crapL___,DiracMatrix[\[Mu]_],crapC___,DiracMatrix[\[Mu]_],crapR___]:>FermionChain[crapL,DiracSandwich[crapC],crapR]};
FeynSubstitute`Rule[10]={MatrixTrace[crapL___,DiracMatrix[\[Mu]_],crapC___,DiracMatrix[\[Mu]_],crapR___]:>If[
Length[{crapL,crapR}]>Length[{crapC}],
MatrixTrace[crapL,DiracSandwich[crapC],crapR],
MatrixTrace[crapC,DiracSandwich[crapR,crapL]]
]};
(*Total number of substitutions - please change*)
FeynSubstitute`N=10;


(* ::Subsubsubsection::Closed:: *)
(*FeynSubstitute command*)


FeynSubstituteF[i_Integer][Expr_]:=ReplaceRepeated[Expr,FeynSubstitute`Rule[i]];
FeynSubstitute[Expr_]:=
Apply[Composition,FeynSubstituteF/@Reverse[Range[FeynSubstitute`N]]][Expr];
FeynSubstituteRepeated[Expr_]:=FixedPoint[FeynSubstitute,Expr];


(* ::Subsubsubsection::Closed:: *)
(*FeynSimplify command*)


FeynSimplify[Expr_,Options___]:=FixedPoint[
Simplify[
FeynSubstitute[#],
TransformationFunctions->Join[{Automatic},FeynTransformation/@Range[FeynTransformationN]],
Options
]&,
Expr
];


(* ::Subsubsubsection::Closed:: *)
(*FeynExpand command*)


FeynExpand[Expr_,Options___]:=Expand[
FeynSubstitute[Expr]/.VectorExpand/.FermionChainExpand/.DiracSlashExpand/.MatrixTraceExpand/.
{crap_:>Expand[crap,Options]}/.{FermionChain[crapL___,DiracSlash[crap_Plus],crapR___]:>Map[FermionChain[crapL,DiracSlash[#],crapR]&,crap]}/.{FermionChain[crapL___,crap_Plus,crapR___]:>FermionChain[crapL,Expand[crap,Options],crapR]}
];
FeynExpandRepeated[Expr_,Options___]:=FixedPoint[FeynExpand[#,Options]&,Expr];


(* ::Subsubsection:: *)
(*Nice notation*)


(* ::Subsubsubsection::Closed:: *)
(*Output formatting*)


(*Generic symbols*)
Format[FeynAmpDenominator[a___]]:=Times[a];
Format[MyDim]:="d";


(*Standard Model*)
Format[SW]:="sin(\!\(\*SubscriptBox[\(\[Theta]\), \(W\)]\))";
Format[MW]:="\!\(\*SubscriptBox[\(M\), \(W\)]\)";
Format[MH]:="\!\(\*SubscriptBox[\(m\), \(H\)]\)";
Format[MB]:="\!\(\*SubscriptBox[\(m\), \(b\)]\)";
Format[YB]:="\!\(\*SubscriptBox[\(y\), \(b\)]\)";
Format[EL]:="e";
Format[GS]:="\!\(\*SubscriptBox[\(g\), \(s\)]\)";
Format[Alfa]:="\[Alpha]";
Format[Alfas]:="\!\(\*SubscriptBox[\(\[Alpha]\), \(s\)]\)";
Format[V[5,__]]:="g";


(*Hierarchy Yukawa theory*)
Format[MQ]:="\!\(\*SubscriptBox[\(m\), \(Q\)]\)";
Format[Mq]:="\!\(\*SubscriptBox[\(m\), \(q\)]\)";
Format[MH]:="\!\(\*SubscriptBox[\(m\), \(H\)]\)";
Format[Yq]:="\!\(\*SubscriptBox[\(y\), \(q\)]\)";
Format[YQ]:="\!\(\*SubscriptBox[\(y\), \(Q\)]\)";
Format[Lambda]:="\[Lambda]";


(* ::Subsubsubsection::Closed:: *)
(*Coupling constant substitutions*)


ElCoupling=Solve[Alfa==EL^2/(4\[Pi]),EL][[1]];
StrongCoupling=Solve[Alfas==GS^2/(4\[Pi]),GS][[1]];


(* ::Section:: *)
(*Physical processes*)


(* ::Subsection:: *)
(*Process and kinematics*)


(* ::Subsubsection:: *)
(*Process definitions*)


(* ::Subsubsubsection::Closed:: *)
(*Process statement*)


Process::usage="Process is the head used to store information about a given physical process.";
Process::undefined="No process was defined for this name.";
Process[Name_]:=Module[{},Message[Process::undefined];Throw[Null]];


DeclareProcess::usage="DeclareProcess[Name,In\[Rule]Out,Model] is used to store information about a process so that using its name all relevant information can be retrieved.";
DeclareProcess[Name_,InParticles_List->OutParticles_List,Theory_String]:=Process[Name]={InParticles->OutParticles,Theory};
DeclareProcess[Name_,InParticles_List->OutParticles_List,Theory_Symbol]:=Process[Name]={InParticles->OutParticles,ToString[Theory]};
DeclareProcess[Crap___]:=Message[DeclareProcess::args];


(* ::Subsubsubsection::Closed:: *)
(*List->List notation*)


ListToListForm[ProcessName_]:=Catch[Process[ProcessName][[1]]];


(* ::Subsubsubsection::Closed:: *)
(*Theory of a process*)


Theory[ProcessName_]:=Catch[Process[ProcessName][[2]]];


(* ::Subsubsubsection::Closed:: *)
(*Integer->Integer notation*)


NToNForm[InParticles_List->OutParticles_List]:=Length[InParticles]->Length[OutParticles];
NToNForm[ProcessName_]:=Catch[NToNForm[ListToListForm[ProcessName]]];
NToNForm[Null]=Null;


(* ::Subsubsubsection::Closed:: *)
(*External legs*)


ExternalLegs[InN_Integer->OutN_Integer]:=InN+OutN;
ExternalLegs[ProcessName_]:=ExternalLegs[NToNForm[ProcessName]];
ExternalLegs[Null]=Null;


(* ::Subsubsubsection::Closed:: *)
(*Recognize constant factors*)


ConstTest[Expr_]:=FreeQ[Expr,Lorentz]&&FreeQ[Expr,SP];
ConstFactor[Process_][NLoop_Integer]:= Select[Simplify[Apply[Plus,Select[#,ConstTest]Unique[a]&/@MyAmplitude[Process][NLoop]]],FreeQ[#,Plus]&];


(* ::Subsubsection:: *)
(*Kinematics*)


(* ::Subsubsubsection::Closed:: *)
(*Shorthands for momenta*)


Momenta::usage="Momenta[\!\(\*SubscriptBox[\(N\), \(in\)]\)\[Rule]\!\(\*SubscriptBox[\(N\), \(out\)]\),\!\(\*SubscriptBox[\(N\), \(loop\)]\):0] is the list of momenta in the form FourMomentum[Incoming/Outgoing/Internal,N].\nMomenta[\!\(\*SubscriptBox[\(N\), \(ext\)]\),\!\(\*SubscriptBox[\(N\), \(loop\)]\):0] is a list of momenta in the form FourMomentum[External/Internal,N].";
Momenta[n_Integer->m_Integer,l_Integer:0]:=Join[Table[FourMomentum[Incoming,j],{j,1,n}],Table[FourMomentum[Outgoing,j],{j,1,m}],Table[FourMomentum[Internal,j],{j,1,l}]];
Momenta[Process_,InOut,l_Integer:0]:=Momenta[NToNForm[Process,l]];
Momenta[n_Integer,l_Integer:0]:=Join[Table[FourMomentum[External,j],{j,1,n}],Table[FourMomentum[Internal,j],{j,1,l}]];
Momenta[Process_,Ext,L_Integer:0]:=Momenta[ExternalLegs[Process],L];
ConvertMomenta::usage="ConvertMomenta[\!\(\*SubscriptBox[\(N\), \(in\)]\)\[Rule]\!\(\*SubscriptBox[\(N\), \(out\)]\)][Expr] replaces external momenta specifying whether they are incoming or outgoing (or vice versa), assuming a process of the form \!\(\*SubscriptBox[\(N\), \(in\)]\)\[Rule]\!\(\*SubscriptBox[\(N\), \(out\)]\).";
ConvertMomenta[NIn_Integer->NOut_Integer][Expr_]:=
ReplaceAll[
Evaluate[
ReplaceAll[
Expr,
{FourMomentum[External,n_Integer]:>If[n<=NIn,FourMomentum[Incoming,n],-FourMomentum[Outgoing,n-NIn]]}
]
],
{Rule[-A_,B_]:>Rule[A,-B]}
]/;And[FreeQ[Expr,Incoming],FreeQ[Expr,Outgoing]];
ConvertMomenta[NIn_Integer->NOut_Integer][Expr_]:=
ReplaceAll[
Evaluate[
ReplaceAll[
Expr,
{FourMomentum[Incoming,n_Integer]:>FourMomentum[External,n],FourMomentum[Outgoing,n_Integer]:>-FourMomentum[External,n+NIn]}
]
],
{Rule[-A_,B_]:>Rule[A,-B]}
]/;FreeQ[Expr,External];


(* ::Subsubsubsection::Closed:: *)
(*Momentum elimination via conservation*)


EliminateMomentum[i_Integer,N_Integer][Expr_]:=Expr/.Solve[Apply[Plus,FourMomentum[External,#]&/@Range[N]]==0,FourMomentum[External,i]][[1]];


(* ::Subsubsubsection::Closed:: *)
(*Momentum conservation*)


LinCombOfAllQ[Vars_List][Expr_]:=If[Head[Expr]=!=Plus,False,Apply[And,Or@@Table[MatchQ[Expr[[i]],_Integer #|#],{i,1,Length[Expr]}]&/@Vars]];
ConserveMomentum[NExt_Integer]:={
a_:>
Module[
{
npos=Count[a,FourMomentum[External,_Integer]|_?Positive FourMomentum[External,_Integer]],
nneg=Count[a,_?Negative FourMomentum[External,_Integer]]
},
If[
(*Warning: this only works for non-fancy coefficients which are mostly 1...*)
npos>nneg,
a-Apply[Plus,FourMomentum[External,#]&/@Range[4]],
a+Apply[Plus,FourMomentum[External,#]&/@Range[4]]
]
]/;LinCombOfAllQ[FourMomentum[External,#]&/@Range[NExt]][a],
(*HACK grrrrrrrr...... only works for Eta*)
a_:>SP[Eta,ReplaceAll[a/.SP[Eta,b_]:>b,ConserveMomentum[NExt]]]/;LinCombOfAllQ[SP[Eta,FourMomentum[External,#]]&/@Range[NExt]][a]
};
(*Warning: this is even more approximate... redefine in terms of the one above!*)
ConserveMomentum[NIn_Integer->NOut_Integer]:=
Module[
{
Foo=Rule[Plus@@ReplaceAll[Momenta[NIn->NOut],{FourMomentum[Outgoing,n_]:>-FourMomentum[Outgoing,n]}],0]
},
{Foo,Times[-1,#]&/@Foo}
];


(* ::Subsubsubsection::Closed:: *)
(*On-shell substitution rules*)


OnShell::usage="OnShell[In\[Rule]Out] is a list of replacements that explicitly sets the squares of the momenta on external legs to the squares of the masses of the corresponding particles.";
OnShell[In_List->Out_List,Verbose_:False]:=Module[
{
ParticleList=Join[In,Out],
System
},
System=And@@Table[SQ[FourMomentum[External,i]]==Power[TheMass[ParticleList[[i]]],2],{i,1,Length[ParticleList]}];
If[Verbose,Print[ParticleList];Print[System]];
Solve[
System,
Table[SP[FourMomentum[External,j],FourMomentum[External,j]],{j,1,Length[ParticleList]}]
][[1]]
];
OnShell[Process_,Verbose_:False]:=OnShell[ListToListForm[Process],Verbose];
OnShell[Null]=Null;


(* ::Subsubsubsection::Closed:: *)
(*On-shell environment setting and unsetting*)


WorkOnShell[Process_]:=
Module[
{},
InitializeModel[Theory[Process]];
Set@@@OnShell[Process]
];
ClearOnShell:=Unset/@Select[
ReplaceAll[#[[1]]&/@DownValues[SP],HoldPattern->Unevaluated],
MatchQ[Unevaluated[#],SP[FourMomentum[External,n_Integer],FourMomentum[External,n_Integer]]]&
];


(* ::Subsubsubsection::Closed:: *)
(*Kinematics substitution setup*)


KinSubs[Null,___]=Null;
KinSubs[Process_]:=KinSubs[NToNForm[Process]]/;Not[NumericQ[Process]]&&Not[MatchQ[Process,_Integer->_Integer]];
KinSubs[Process_,NoVar_Symbol]:=KinSubs[NToNForm[Process],NoVar]/;Not[NumericQ[Process]]&&Not[MatchQ[Process,_Integer->_Integer]];


KinSubs[NExt_Integer,NElim_Integer,{MElim_Integer,LElim_Integer}]:=
Solve[
And@@Append[
Table[SQ[Apply[Plus,k/@Select[Range[NExt],#=!=i&&#=!=NElim&]]]==SQ[k[i]+k[NElim]],{i,Select[Range[NExt],#=!=NElim&]}],
SQ[Apply[Plus,k/@Select[Range[NExt],#=!=NElim&]]]==SQ[k[NElim]]
],
Append[
SP[k[NElim],k[#]]&/@Select[Range[NExt],#=!=NElim&],
SP[k[MElim],k[LElim]]
]
][[1]]/.{k[i_Integer]:>FourMomentum[External,i]};
KinSubs[NExt_Integer,NElim_Integer]:=KinSubs[NExt,NElim,{NExt-2,NExt-1}];
KinSubs[NExt_Integer,{MElim_Integer,LElim_Integer}]:=KinSubs[NExt,NExt,{MElim,LElim}];
KinSubs[NExt_Integer]:=KinSubs[NExt,NExt,{NExt-2,NExt-1}];


(* ::Subsubsubsection::Closed:: *)
(*Kinematics for 2->1 processes*)


KinSubs[2->1]=KinSubs[3];


(* ::Subsubsubsection::Closed:: *)
(*Kinematics for 2->2 processes*)


KinSubs[2->2]=
Evaluate[
Solve[
And[
S==SQ[k[1]+k[2]]==SQ[k[3]+k[4]],
T==SQ[k[3]+k[1]]==SQ[k[2]+k[4]],
U==SQ[k[4]+k[1]]==SQ[k[2]+k[3]]
],
Flatten[Table[SP[k[i],k[j]],{i,1,4},{j,i+1,4}]]
][[1]]/.{k[i_Integer]:>FourMomentum[External,i]}
];
KinSubs[2->2,NoVar_Symbol]:=Module[
{
VarSub=Solve[S+T+U==Plus@@Map[SQ[FourMomentum[External,#]]&,Range[4]],NoVar][[1]]
},
Join[
KinSubs[2->2]/.VarSub,
VarSub
]
];


(* ::Subsubsubsection::Closed:: *)
(*Mandelstam-like invariants*)


SetAttributes[Mandelstam,Orderless];
Format[Mandelstam[a_,b_]]:=Subscript["s", ToString[a]<>ToString[b]];
Mandelstamize[Massless]:=SP[FourMomentum[External,a_],FourMomentum[External,b_]]:>Mandelstam[a,b]/2;


(* ::Subsection:: *)
(*Diagram generation*)


(* ::Subsubsection::Closed:: *)
(*Command*)


(* ::Text:: *)
(*Note: Topologies are stored in MyTopologies[Process][Number of loops]*)


ClearAll[MyTopologies];
MyTopologies[Process_Rule,MyInsertionLevel_:{Classes},NOTops_List:{Tadpoles,WFCorrections}][LoopN_Integer]:=InsertFields[CreateTopologies[LoopN,NToNForm[Process],ExcludeTopologies->NOTops],ListToListForm[Process],Model->Theory[Process],GenericModel->"LorentzGeneric",InsertionLevel->MyInsertionLevel];
MyTopologies[Process_Rule,MyInsertionLevel_,NOTops_List,Options__Rule][LoopN_Integer]:=InsertFields[CreateTopologies[LoopN,NToNForm[Process],ExcludeTopologies->NOTops],ListToListForm[Process],Model->Theory[Process],GenericModel->"LorentzGeneric",InsertionLevel->MyInsertionLevel,Options];


(* ::Subsection:: *)
(*Integral topologies*)


(* ::Subsubsection:: *)
(*Shifts*)


(* ::Subsubsubsection::Closed:: *)
(*Find denominators containing loop momenta*)


Shift`ExtQ[Expr_]:=FreeQ[Expr,Internal];
Shift`LoopQ[Expr_]:=!FreeQ[Expr,Internal];
Shift`LoopDenominators[Amplitude_]:=FixedPoint[Replace[#,Power[a_,n_]:>a,{1}]&,Cases[Denominator[Amplitude],a_?Shift`LoopQ]];


(* ::Subsubsubsection::Closed:: *)
(*Find loop or external momenta in an expression*)


Shift`LoopMomenta[Expr_]:=DeleteDuplicates[Cases[Expr,FourMomentum[Internal,_Integer],Infinity]];
Shift`LoopN[Expr_]:=Apply[Max,#[[2]]&/@Shift`LoopMomenta[Expr]];
Shift`ExternalMomenta[Expr_]:=DeleteDuplicates[Cases[Expr,FourMomentum[External,_Integer],Infinity]];


(* ::Subsubsubsection::Closed:: *)
(*Manipulate quadratic functions*)


ClearAll[Shift`ABCForm];
Shift`ABCForm[SP[FourMomentum[Internal,i_Integer],FourMomentum[Internal,i_Integer]],Dim_Integer]:={Normal[SparseArray[{{i,i}->1},{Dim,Dim}]],Normal[SparseArray[{},Dim]],0};
Shift`ABCForm[SP[FourMomentum[Internal,i_Integer],FourMomentum[Internal,j_Integer]],Dim_Integer]:={Normal[SparseArray[{{i,j}->1/2,{j,i}->1/2},{Dim,Dim}]],Normal[SparseArray[{},Dim]],0};
Shift`ABCForm[SP[a_?Shift`ExtQ,FourMomentum[Internal,j_Integer]],Dim_Integer]:={Normal[SparseArray[{},{Dim,Dim}]],Normal[SparseArray[{j->a},Dim]],0};
Shift`ABCForm[SP[FourMomentum[Internal,j_Integer],a_?Shift`ExtQ],Dim_Integer]:={Normal[SparseArray[{},{Dim,Dim}]],Normal[SparseArray[{j->a},Dim]],0};
Shift`ABCForm[a_?Shift`ExtQ,Dim_Integer]:={Normal[SparseArray[{},{Dim,Dim}]],Normal[SparseArray[{},Dim]],a};
Shift`ABCForm[Expr_Plus,Dim_Integer]:=Shift`ABCForm[#,Dim]&/@Expr;
Shift`ABCForm[a_?Shift`ExtQ Expr_,Dim_Integer]:=a Shift`ABCForm[Expr,Dim];
SP[Shift`m[i__Integer]a_,b_]:=Shift`m[i]SP[a,b];


(*!!!!!!!!!!!!!!!!!!!! WARNING: This is NOT guaranteed to work beyond one variable !!!!!!!!!!!!!!!!!!!!!!*)
Format[Shift`Fudge]:="c";
Shift`LinearQ[Expr_]:=FreeQ[Expr,SP[FourMomentum[Internal,_Integer],FourMomentum[Internal,_Integer]]];
Shift`ABCSolve[LHS_==RHS_,Unknowns_List,Verbose_:False]:=
Module[
{
LoopN=Shift`LoopN[LHS==RHS],
ABCLHS,ABCRHS,
System,Foo
},
If[Verbose,Print["ABCSolve called with ",LHS==RHS," and ",Unknowns,"!"]];
ABCLHS=Shift`ABCForm[LHS,LoopN];
ABCRHS=Shift`ABCForm[RHS,LoopN];
If[Verbose,Print["The matrix form of the system I am going to solve is"]];
If[Verbose,
Print[
MatrixForm[ABCLHS[[1]]]==MatrixForm[ABCRHS[[1]]]," && ",
MatrixForm[ABCLHS[[2]]]==MatrixForm[ABCRHS[[2]]]," && ",
ABCLHS[[3]]==ABCRHS[[3]]]];
If[
Shift`LinearQ[LHS]=!=Shift`LinearQ[RHS],
If[Verbose,Print["The system has no solution."]];
{},
If[
Shift`LinearQ[LHS],
(*Solution in case the system is linear*)
If[Verbose,Print["The system is linear in the loop momenta."]];
System=ABCLHS[[2]]==ABCRHS[[2]]&&ReplaceAll[ABCLHS[[3]]==ABCRHS[[3]],SP->Times];
Foo=Solve[System,Unknowns];
If[Verbose,Print["The solution may be ",Foo]];
Select[Foo,Reduce[ReplaceAll[ABCLHS[[3]]==ABCRHS[[3]],#]]&],
(*Solution in case the system is quadratic*)
If[Verbose,Print["The system is quadratic in the loop momenta."]];
System=ABCLHS[[1]]==ABCRHS[[1]]&&ABCLHS[[2]]==ABCRHS[[2]](*&&And@@Map[#\[Equal]1||#==-1&,Flatten[Array[Shift`m,{LoopN,LoopN}]]]*);
(*RealUnknowns=Join[Flatten[Array[Shift`m,{LoopN,LoopN}]],Shift`q/@Range[LoopN],{Shift`Fudge}];*)
Foo=Solve[System,Unknowns];
If[Verbose,Print["Unparsed solutions are ",Foo]];
Select[Foo,Reduce[ReplaceAll[ABCLHS[[3]]==ABCRHS[[3]],#]]&]
]
]
];


(*Test*)
Shift`ABCSolve[SQ[FourMomentum[Internal,1]+FourMomentum[External,17]](-1)==Fudge SQ[FourMomentum[Internal,1]+c FourMomentum[External,17]]//.SP[c a_,b_]:>c SP[a,b],{c,Fudge}];
Shift`ABCSolve[SP[FourMomentum[Internal,1],FourMomentum[External,17]]==c SP[FourMomentum[Internal,1], FourMomentum[External,17]],{c}];


(* ::Subsubsubsection::Closed:: *)
(*Shifting loop momenta*)


Format[FourMomentum[Shifted,i_Integer]]:=Subsuperscript[k,i,","];
Format[Shift`m[i__Integer]]:=Subscript["\[Sigma]",Apply[StringJoin,ToString/@{i}]];
Format[Shift`q[i_Integer]]:=Subscript["l",ToString[i]];
Shift`Shift[Expr_,LoopN_Integer]:=Expr/.FourMomentum[Internal,i_Integer]:>Plus@@Table[Shift`m[i,j]FourMomentum[Internal,j],{j,1,LoopN}]+Shift`q[i];


(* ::Subsubsubsection::Closed:: *)
(*Fudge factor convention*)


Shift`FudgeConvention[Denoms_List]:=Shift`FudgeConvention/@Denoms;
Shift`FudgeConvention[Denom_,Verbose_:False]:=If[
!Shift`LinearQ[Denom],
If[Verbose,Print["Quadratic denominator"]];
Denom/Det[Shift`ABCForm[Denom,Shift`LoopN[Denom]][[1]]],
If[Verbose,Print["Linear denominator"]];
Module[
{
MyConstTerm=Shift`ABCForm[Denom,Shift`LoopN[Denom]][[3]],
MyMom=Shift`ABCForm[Denom,Shift`LoopN[Denom]][[2]]
},
If[
MyConstTerm===0,
Expand[2Denom/Coefficient[Plus@@MyMom,FourMomentum[External,Min[Cases[MyMom,FourMomentum[External,_],Infinity]/.FourMomentum[External,i_]:>i]]]],
Expand[2Denom/Coefficient[MyConstTerm,Cases[{MyConstTerm},SP[_,_],Infinity][[1]]]]
]
]
]/;MatchQ[Denom,_Times|_Plus|_SP];


(* ::Subsubsubsection::Closed:: *)
(*Find the shift that maps a certain set of denominators into another*)


Shift`FindShift[IDenoms_List,IConfigurationDenoms_List,Verbose_:False]:=
Module[
{
Denoms=Shift`FudgeConvention[IDenoms],
ConfigurationDenoms=Shift`FudgeConvention[IConfigurationDenoms],
Unknowns=Shift`LoopMomenta[IDenoms],
Parameters=Shift`ExternalMomenta[Join[IDenoms,IConfigurationDenoms]],
RealUnknowns,
NLoop,
Systems,
CandidateShifts,
GoodQ,
Foo
},
(*Determine effective number of loops from the number of loop momenta found*)
NLoop=Length[Unknowns];
RealUnknowns=Flatten[{Array[Shift`m,{NLoop,NLoop}],Array[Shift`q,{NLoop}]}];
(*Print the shifted candidate and the unknowns found*)
If[Verbose,Print["Shifted candidate is: ",Denoms/.Internal->Shifted]];
If[Verbose,Print["There are ",NLoop," unknowns: ",Rule[#/.Internal->Shifted,Shift`Shift[#,NLoop]]&/@Unknowns]];
If[Verbose,Print["The following momenta will be treated as parameters: ",Parameters]];
(*Possible good shifts are determined by...*)
Systems=Map[
(*...the system of equations where the first NLoop shifted propagators...*)
Inner[Equal,Take[Shift`Shift[Denoms,NLoop],NLoop],#,And]&,
(*...are set equal to any NLoop propagators of the given configuration.*)
Permutations[ConfigurationDenoms,{NLoop}]
]//Expand;
(*Print the systems of equations*)
If[Verbose,Print["I will attempt to solve the following systems: ",Systems//MatrixForm]];
(*Solve all systems*)
CandidateShifts=Flatten[Map[Shift`ABCSolve[#,RealUnknowns]&,Systems],1];
(*Print the solutions*)
If[Verbose,Print["Unparsed shifts are: ",Rule[#/.Internal->Shifted,Shift`Shift[#,NLoop]]&/@Unknowns/.CandidateShifts//MatrixForm]];
(*Check the remaining denominators*)
GoodQ[Expr_]:=MySubsetQ[
Expand[ReplaceAll[Shift`Shift[Denoms,NLoop],Expr]],
Expand[ConfigurationDenoms],
ProportionalQ
];
If[Verbose,
Print[
MatrixForm[Expand[ReplaceAll[Shift`Shift[Denoms,NLoop],#]]&/@CandidateShifts],
" \[Subset] ",
Expand[ConfigurationDenoms],
" ? ",
MatrixForm[GoodQ/@CandidateShifts]
]
];
Foo=Select[CandidateShifts,GoodQ];
If[
Foo==={},
{},
ReplaceAll[
Rule[#,Shift`Shift[#,NLoop]]&/@Unknowns,
Foo
]
]
];


(*Test*)
Shift`FindShift[{SP[FourMomentum[Internal,1],FourMomentum[Internal,1]]},{SP[FourMomentum[Internal,1]+FourMomentum[External,1],FourMomentum[Internal,1]+FourMomentum[External,1]]}];


(* ::Subsubsubsection::Closed:: *)
(*Find the topology that corresponds to a set of denominators*)


Shift`FindTopology[Denoms_List,Topologies_List,Verbose_:False,VeryVerbose_:False]:=
Module[
{
MyShifts={},
i=0
},
If[Verbose,Print["\n\nCandidate: ",Denoms]];
While[
MyShifts=={}&&i<Length[Topologies],
++i;
If[Verbose,Print["\nChecking configuration N. ",i,": ",Topologies[[i]]]];
MyShifts=Shift`FindShift[Denoms,Topologies[[i]],VeryVerbose]
];
If[
MyShifts=!={},
If[Verbose,Print["Shift(s) ",MyShifts," found."]];
MyShifts=MyShifts[[1]],
If[Verbose,Print["No good shift found."]];
{}
]
];


(* ::Subsubsubsection::Closed:: *)
(*Absorbing denominators*)


Shift`AbsorbDenominators[Expr_]:=
ReplaceRepeated[
Expr,
{
Integral[Top[i_Integer]][\[Nu]__Integer]/MyDen_:>Module[
{
Match=ProportionalQ[Expand[MyDen],#]&/@Denominators[Top[i]],
MatchDen,Pos
},
MatchDen=Pick[Denominators[Top[i]],Match][[1]];
Pos=Position[Match,True][[1]];
Times[
Integral[Top[i]]@@Plus[{\[Nu]},SparseArray[Pos->1,Length[{\[Nu]}],0]],
Simplify[MatchDen/MyDen]
]
]/;Apply[Or,ProportionalQ[Expand[MyDen],#]&/@Denominators[Top[i]]],
Integral[Top[i_Integer]][\[Nu]__Integer]Power[MyDen_,n_]:>Module[
{
Match=ProportionalQ[Expand[MyDen],#]&/@Denominators[Top[i]],
MatchDen,Pos
},
MatchDen=Pick[Denominators[Top[i]],Match][[1]];
Pos=Position[Match,True][[1]];
Times[
Integral[Top[i]]@@Plus[{\[Nu]},SparseArray[Pos->-n,Length[{\[Nu]}],0]],
Simplify[MatchDen/MyDen]
]
]/;Apply[Or,ProportionalQ[Expand[MyDen],#]&/@Denominators[Top[i]]]
}
];


(* ::Subsubsubsection::Closed:: *)
(*Shift command*)


ClearAll[Denominators,Shift];
Denominators::usage="Denominators[Top] gives back the denominators in the topology labeled by Top.";
Shift::usage="Shift[Amplitudes,TopList] finds the minimal set of topologies from the given Amplitudes.
It also performs on said Amplitudes the appropriate shifts on loop momenta and substitutes them with placeholders stored in TopList.";
Shift::inout="Please convert Incoming- and Outgoing-type momenta to generic External-type momenta before calling Shift.";
Shift::denoms="Warning: clearing old topologies...";
Shift[Amplitudes_,StartingTopologies_List:{},Verbose_:False,VeryVerbose_:False]:=
(*If Amplitudes contain Incoming/Outgoing type momenta, prompt a message and quit*)
If[
!And[FreeQ[Amplitudes,Incoming],FreeQ[Amplitudes,Outgoing]],
Message[Shift::inout],
If[Verbose,Print["Starting Shift procedure"]];
(*Recursively find topologies*)
Module[
{
Candidates=Expand[Apply[List,Shift`LoopDenominators/@Amplitudes]],
UpdateTopList,
Topologies=StartingTopologies,
MissingMomenta={},
FooAmps,FooQ,FooTops
},
UpdateTopList[MyList_List,NewTop_List]:=
CompoundExpression[
If[Verbose,Print["Updating topology list..."]],
Select[MyList,Shift`FindShift[#,NewTop,VeryVerbose]==={}&]
];
If[Verbose,Print["Candidates are: ",Candidates//MatrixForm]];
Map[
If[
Shift`FindTopology[#1,Topologies,Verbose,VeryVerbose]==={},
Topologies=Append[UpdateTopList[Topologies,#1],Shift`FudgeConvention[#1]]
]&,
Candidates
];
If[Verbose,Print["At the end of the day my topologies are",Topologies]];
(*Store information about the topologies to be called with Denominators*)
If[DownValues[Denominators]=!={},Message[Shift::denoms];Clear[Denominators]];
Table[Denominators[Top[i]]=Topologies[[i]],{i,1,Length[Topologies]}];
(*Shift amplitudes*)
FooAmps=ReplaceAll@@@Transpose[{Amplitudes,Shift`FindTopology[#,Topologies]&/@Candidates}];
FooQ[Expr_][n_Integer]:=MySubsetQ[Expand[Shift`LoopDenominators[Expr]],Denominators[Top[n]],ProportionalQ];
FooTops=Map[#[[1]]&,Select[Range[Length[Topologies]],FooQ[#]]&/@FooAmps];
FooAmps=Table[FooAmps[[i]]Integral[Top[FooTops[[i]]]]@@ConstantArray[0,Length[Denominators[Top[FooTops[[i]]]]]],{i,1,Length[FooTops]}];
(*Absorb denominators into Integral symbol*)
Shift`AbsorbDenominators[FooAmps]
]
];


(* ::Subsubsection::Closed:: *)
(*Partial fractioning*)


PartialFraction[Expr_,ExtraPars_:{},MyHead_:Plus,Verbose_:True]:=Module[
{
Denoms=Shift`LoopDenominators[Expr],
DenomsNonConst,
Pars,
c,
Sol,
Systems,
cs
},
Format[c[i_Integer]]:=Subscript["c",i];
If[Verbose,Print["The denominators are: ",Denoms]];
DenomsNonConst=Select[#,Shift`LoopQ]&/@Plus[Denoms,1];
If[Verbose,Print["The pieces of denominators that depend on the loop momenta are: ",DenomsNonConst]];
Pars=Join[DeleteDuplicates[Cases[Denoms,_SP,Infinity]],ExtraPars];
cs=Array[c,Length[Denoms]];
If[Verbose,Print["I will interpret ",Pars," as parameters, and ",cs," as unknowns, and try to solve\n",cs.DenomsNonConst==0]];
Sol=SolveAlways[
cs.DenomsNonConst==0,
Pars
];
If[Verbose,Print["Preliminary solutions are: ",Sol]];
If[
Length[Sol]===0||Union[Flatten[cs/.Sol]]==={0},
Expr,
If[
(*Check if there is a solution without constant term*)
(*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!WARNING: handling of multiple solutions missing!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)
Reduce[ReplaceAll[cs.Denoms==cs.DenomsNonConst,Sol[[1]]]]===True,
(*No constant term*)
Module[
{
DoubleList=SortBy[
Select[Transpose[{Denoms,cs}]/.Sol[[1]],#[[2]]=!=0&],
SimplifyCount[#[[1]]]&
]
},
If[Verbose,Print["I think that ",Dot@@Transpose[Rest[DoubleList]]/(-Times@@First[DoubleList])," = ",Simplify[Dot@@Transpose[Rest[DoubleList]]/(-Times@@First[DoubleList])]," = 1"]];
ReplaceAll[Apply[MyHead,Dot@@Transpose[Rest[DoubleList]]]/(-Times@@First[DoubleList]),c[n_Integer]:>1]Expr
],
(*With constant term*)
Systems=And@@ReplaceAll[#,Rule->Equal]&&cs.Denoms==1&&!Apply[And,#==0&/@cs]&/@Sol;
Sol=Flatten[Solve[#,cs]&/@Systems,1];
If[Verbose,Print["Solutions for partial fractioning are: ",Sol]];
If[
Sol=={},
Expr,
Apply[MyHead,ReplaceAll[cs.Denoms,Sol[[1]]]]Expr
]
]
]
]


(* ::Subsubsection::Closed:: *)
(*Numerator algebra*)


ReduceNumerator::usage="ReduceNumerator rewrites the scalar products that contain the loop momentum as negative powers of the denominators in a configuration.";
ReduceNumerator::multtop="Please feed to ReduceNumerator one topology per time.";
ReduceNumerator::notop="Could not find topology. Did you pass ReduceNumerator a zero?";
ReduceNumerator::noelim="It was impossible to eliminate all loop momenta from the numerator.";
ReduceNumerator[Expr_,Verbose_:False]:=
Module[
{
Tops=Cases[Expr,Integral[___][___],Infinity],
TheTop,
Den,
Denoms,
Unknowns,
Replacements={},
Result
},
Format[Den[i_Integer]]:=Subscript["D",i];
(*Checking the number of configurations in Expr*)
If[
Length[Tops]=!=1,
If[
Length[Tops]===0,
Message[ReduceNumerator::notop],
Message[ReduceNumerator::multtop]
];
Abort
];
TheTop=Tops[[1]][[0]][[1]];
If[Verbose,Print["Considering topology ",TheTop]];
Denoms=Denominators[TheTop];
(*Determining scalar products as a function of Propagator denominators of appropriate momenta*)
Unknowns=DeleteDuplicates[Cases[{Expr,Denoms},SP[FourMomentum[Internal,i_Integer],k_FourMomentum],Infinity]];
If[Verbose,Print["The invariants I will try to replace are: ",Unknowns]];
Replacements=Solve[
And@@Table[Den[i]==Denoms[[i]],{i,1,Length[Denoms]}],
Unknowns
];
(*Print[Table[Den[i]\[Equal]Denoms[[i]],{i,1,Length[Denoms]}]];*)
If[
Replacements==={},
Message[ReduceNumerator::noelim],
If[Verbose,Print["I am going to carry out the following replacements: ",Replacements[[1]]]];
Result=ReplaceRepeated[
Expand[Expr/.Replacements[[1]]],
{
Den[i_Integer]Integral[conf__][\[Nu]s__ ]:> 
Apply[
Integral[conf],
{\[Nu]s}+Normal[SparseArray[{i->-1},Length[{\[Nu]s}]]]
],
Power[Den[i_Integer],n_Integer]Integral[conf__][\[Nu]s__ ]:> 
Apply[
Integral[conf],
{\[Nu]s}+Normal[SparseArray[{i->-n},Length[{\[Nu]s}]]]
]
}
];
(*If[FreeQ[Result,FourMomentum[Internal,_]],*)Simplify[Collect[Result,Integral[___][___],Simplify]](*,Message[ReduceNumerator::noelim]]*)
]
];


(* ::Subsubsection::Closed:: *)
(*Rotating topologies*)


TopologyRotation[i_Integer->j_Integer,Verbose_:False]:=TopologyRotation[Denominators[Top[i]]->Denominators[Top[j]],Verbose];


TopologyRotation::usage="TopologyRotation[Top1\[Rule]Top2] gives a list of momenta relabelings and shifts that maps Top1 in Top2, if this is possible.";
TopologyRotation[Top1_List->Top2_List,Verbose_:False]:=TopologyRotation[Top1->Top2]=
Module[
{
Relabel,
Dummy,
Unknowns,
RHSs,
(*Fudge,*)
L=Length[Shift`LoopMomenta[Join[Top1,Top2]]],
Solution={}
},
(*Format[Fudge[i_Integer]]:=Subscript["c",i];*)
Format[FourMomentum[Dummy,i_Integer]]:=Subscript["l",i];
Relabel[Expr_]:=ReplaceAll[Expr,{External->Dummy,FourMomentum[Internal,i_Integer]:>FourMomentum[Internal,i]+FourMomentum[Dummy,-i]}];
Unknowns=DeleteDuplicates[Cases[Relabel[Top1],FourMomentum[Dummy,_Integer],Infinity]];
If[Verbose,Print["Called TopologyRotation[",Top1->Top2,"]!"]];
RHSs=Permutations[Top2,{Length[Top1]}];
(*RHSs=Inner[Times,Array[Fudge,Length[Top2]],#,List]&/@Permutations[Top2,{Length[Top1]}];
Unknowns=Join[Unknowns,Array[Fudge,Length[Top2]]];*)
While[
Solution==={}&&RHSs=!={},
If[Verbose,Print["Comparing with ",RHSs[[1]]]];
Solution=Module[
{
ABCLHSs=Shift`ABCForm[#,L]&/@Relabel[Top1],
ABCRHSs=Shift`ABCForm[#,L]&/@RHSs[[1]],
System,Foo,Check
},
System=Map[#[[1]]&,ABCLHSs]==Map[#[[1]]&,ABCRHSs]&&Map[#[[2]]&,ABCLHSs]==Map[#[[2]]&,ABCRHSs];
If[Verbose,Print["The system is ",Map[#[[1]]&,ABCLHSs],"\[Equal]",Map[#[[1]]&,ABCRHSs]," && ",Map[#[[2]]&,ABCLHSs],"\[Equal]",Map[#[[2]]&,ABCRHSs],"\nWhich reduces to ",MatrixForm[List@@System]]];
Foo=Solve[System,Unknowns];
Check[Expr_]:=Expand[ReplaceAll[Map[#[[3]]&,ABCLHSs],Expr]]==Expand[Map[#[[3]]&,ABCRHSs]];
Select[Foo,Check]
];
If[Verbose,Print["The solution is ",Solution]];
RHSs=Drop[RHSs,1]
];
If[Solution==={},{},Solution[[1]]/.Dummy->External]
]


(* ::Subsubsection:: *)
(*Tensor reduction*)


(* ::Subsubsubsection::Closed:: *)
(*Absorbing loop momentum tensors*)


ToTensorIntegrals[Expr_]:=
(*For the terms containing Subscript[k, \[Mu]] or k\[NegativeThickSpace]/, do tensor integrals*)
(*Needs to be extended to work beyond one loop*)
ReplaceRepeated[
FeynExpandRepeated[Expr],
{
FermionChain[L___,DiracSlash[FourMomentum[Internal,1]],R___]Integral[Top[n_]][\[Nu]s__]:>Module[{\[Mu]},FermionChain[L,DiracMatrix[\[Mu]],R]TensorIntegral[Top[n]][\[Nu]s][\[Mu]]],
Vector[FourMomentum[Internal,1],\[Mu]_]Integral[Top[n_]][\[Nu]s__]:>TensorIntegral[Top[n]][\[Nu]s][\[Mu]],
Vector[FourMomentum[Internal,1],\[Mu]_]TensorIntegral[Top[n_]][\[Nu]s__][\[Mu]s__]:>TensorIntegral[Top[n]][\[Nu]s][\[Mu]s,\[Mu]]
}
];


(* ::Subsubsubsection::Closed:: *)
(*Tensor structures*)


TensorStructures[Anything__][]:={1};
TensorStructures[BasicMomenta__FourMomentum][\[Mu]__]:=
Module[
{
FooF
},
FooF[a_]:={1,Vector[a,#1]&};
Apply[
TensorStructures,
Append[
Map[
FooF,
{BasicMomenta}
],
{2,MetricTensor[#1,#2]&}
]
][\[Mu]]
];
TensorStructures[BasicStructures__List][\[Mu]__]:=
Module[
{
i={\[Mu]}
},
Table[
If[
Length[i]<{BasicStructures}[[j]][[1]],
{},
Module[
{
IndexSets=Prepend[#,First[i]]&/@Tuples[Rest[i],{{BasicStructures}[[j]][[1]]-1}]
},
Table[
Times[
{BasicStructures}[[j]][[2]]@@IndexSets[[k]],
TensorStructures[BasicStructures]@@Complement[i,IndexSets[[k]]]
],
{k,1,Length[IndexSets]}
]
]
],
{j,1,Length[{BasicStructures}]}
]//Flatten
];


SymmTensorStructures[BasicMomenta__FourMomentum][\[Mu]__]:=
Module[
{
FooF
},
FooF[a_]:={1,Vector[a,#1]&};
Apply[
SymmTensorStructures,
Append[
Map[
FooF,
{BasicMomenta}
],
{2,MetricTensor[#1,#2]&}
]
][\[Mu]]
];
SymmTensorStructures[BasicStructures__List][\[Mu]___]:=
Module[
{
Foo=TensorStructures[BasicStructures][\[Mu]],
PermuteRules=Map[
Rule@@@Transpose[{{\[Mu]},#}]&,
Permutations[{\[Mu]}]
]
},
Foo=DeleteDuplicates[Foo,MySubsetQ[{#1},#2/.PermuteRules]&];
Expand[Apply[Plus,#/.PermuteRules]/(Length[{\[Mu]}]!)&/@Foo]
]


(* ::Subsubsubsubsection:: *)
(*Example and counting tests*)


(* ::Input:: *)
(*TensorStructures[*)
(*{1,Vector[FourMomentum[External,1],#1]&},*)
(*{2,MetricTensor[#1,#2]&}*)
(*][\[Mu],\[Nu],\[Rho],\[Sigma]]*)


(* ::Input:: *)
(*TensorStructures[FourMomentum[External,1],FourMomentum[External,3]][\[Mu],\[Nu],\[Rho]]*)


(* ::Input:: *)
(*SymmTensorStructures[FourMomentum[External,1],FourMomentum[External,2]][\[Mu],\[Nu],\[Rho],\[Sigma]]*)


(* ::Input:: *)
(*Length[%]*)


(* ::Subsubsubsection::Closed:: *)
(*Passarino-Veltman routine*)


TensorReduction[Expr_,Verbose_:False]:=
Expr/.{
TensorIntegral[Topology_][\[Nu]s__][\[Mu]s__]:>
Module[
{
Structures,c,Den,UnknownSPs,
Denoms=Denominators[Topology],
Momenta,
Integrand,Result,
DenSubs,Tmp,
LHS,LHSs,RHSs
},
Integrand=Apply[
Times,
Vector[FourMomentum[Internal,1],#]&/@{\[Mu]s}
]/Apply[Times,Power@@@Transpose[{Denoms,{\[Nu]s}}]];
If[Verbose,Print["The integrand is ",Integrand]];
Format[c[m_Integer]]:=Subscript["c",m];
Format[Den[m_Integer]]:=Subscript["D",m];
Format[LHS[m_Integer]]:="LHS"[m];
DenSubs=Solve[
Inner[Equal,Denoms,Den/@Range[Length[Denoms]],And],
DeleteDuplicates[Cases[Denoms,SP[FourMomentum[Internal,i_Integer],Stuff_],Infinity]]
][[1]];
If[Verbose,Print["I will substitute scalar products with ",DenSubs]];
Momenta=DeleteDuplicates[Cases[Denoms,FourMomentum[External,n_Integer],Infinity]];
Structures=SymmTensorStructures[Sequence@@Momenta][\[Mu]s];
Result=Inner[Times,Array[c,Length[Structures]],Structures,Plus];
If[Verbose,Print["The result will be written in the form: ",Result]];
RHSs=Table[Structures[[i]]Result,{i,1,Length[Structures]}]//Expand;
LHSs=Expand[Simplify[Expand[Table[Structures[[i]]Integrand,{i,1,Length[Structures]}]]/.DenSubs]Apply[Integral[Topology],ConstantArray[0,Length[Denoms]]]]//.{
Integral[Topology][\[Nu]__Integer]Den[i_Integer]:>Integral[Topology]@@Plus[SparseArray[{i->-1},Length[{\[Nu]}],0],{\[Nu]}],
Integral[Topology][\[Nu]__Integer]/Den[i_Integer]:>Integral[Topology]@@Plus[SparseArray[{i->1},Length[{\[Nu]}],0],{\[Nu]}],
Integral[Topology][\[Nu]__Integer]Power[Den[i_Integer],m_]:>Integral[Topology]@@Plus[SparseArray[{i->-m},Length[{\[Nu]}],0],{\[Nu]}]
};
If[Verbose,Print["The system of equations looks like ",Inner[Equal,(*LHSs*)"LHS"/@Range[Length[LHSs]],RHSs,And]]];
Tmp=Expand[
Rule@@@Transpose[{
c/@Range[Length[RHSs]],
LinearSolve[
Table[Coefficient[RHSs[[i]],c[j]],{i,1,Length[RHSs]},{j,1,Length[Structures]}],LHS/@Range[Length[RHSs]]
]/. LHS[i_Integer]:>LHSs[[i]]
}]
];
If[Verbose,Print["I am going to carry out the following substitutions\n",Tmp]];
Result/. Tmp
]
};


(* ::Subsection:: *)
(*Reduction to master integrals*)


(* ::Subsubsection:: *)
(*Generating Water input*)


(* ::Subsubsubsection::Closed:: *)
(*General configuration*)


Water`Path="~/ETH/Reduction/Water/";
Water`Verbose=False;


(* ::Subsubsubsection::Closed:: *)
(*Automatic determination of parameters*)


Water`AutoConf[Expr_,Verbose_:Water`Verbose]:=Module[
{
ExpList=
Apply[
List,
Gather[
Cases[Expr,Integral[Top[_Integer]][___],Infinity],
TopologyRotation[#1[[0]][[1]][[1]]->#2[[0]][[1]][[1]]]=!={}&
],
{2}
],
FooSelect,
ExpList2
},
If[Verbose,Print[ExpList]];
FooSelect[x_List]:=Select[x,#1=!=0&&#1=!=-1&];
ExpList2=Map[FooSelect,-ExpList,{2}];
If[Verbose,Print[ExpList2]];
Water`MaxPos=Table[Apply[Max,Map[Plus@@Cases[#,x_?Positive]&,ExpList2[[i]]]],{i,1,Length[ExpList]}];
(*Warning: +2 fugde here...*)
Water`MaxNeg=Table[-Apply[Min,Map[Plus@@Cases[#,x_?Negative]&,ExpList2[[i]]]],{i,1,Length[ExpList]}]+2;
(*Water`MaxTotNegative=Table[-Apply[Min,Map[Min@@#&,1-ExpList[[i]]]],{i,1,Length[ExpList]}];*)
Water`MaxNumOfProps=Table[Max[Count[#,x_?Positive]&/@ExpList[[i]]],{i,1,Length[ExpList]}];
Null
];


(* ::Subsubsubsection::Closed:: *)
(*Code snippets*)


Water`Main`Head="
//This File was automatically generated from Mathematica
#include \"includes/Water.h\"

int main (int argc, char * argv[])
{
   gargc=argc;
   gargv=argv;
   string path=\"../test/\";
   programswitch=Ferm;
   vector<ex> loopmom;
   vector<Denominator> denoms;
";
Water`Main`Tail="   water->Run();
   water->ExportIntegrals(path+\"Reductions/\"+mytop+\".txt\");

   return 0;
}
";


(* ::Subsubsubsection::Closed:: *)
(*Conversion of expressions to strings*)


(* ::Text:: *)
(*Note: this assumes that the environment has been set with WorkOnShell*)


Water`ToString[SP[FourMomentum[External,i_Integer],FourMomentum[External,j_Integer]],Flag_:False]:=Water`ToString[FourMomentum[External,i]]<>If[Flag,"*",""]<>Water`ToString[FourMomentum[External,j]];
Water`ToString[SP[a_,b_],Flag_:False]:=Water`ToString[a]<>"*"<>Water`ToString[b];
(*This Dot business is not really clean...*)
Water`ToString[Dot[FourMomentum[External,i_Integer],FourMomentum[External,j_Integer]],Flag_:False]:=Water`ToString[FourMomentum[External,i]]<>If[Flag,"*",""]<>Water`ToString[FourMomentum[External,j]];
Water`ToString[Dot[a_,b_],Flag_:False]:=Water`ToString[a]<>"*"<>Water`ToString[b];
Water`ToString[a_-b_,Flag_:False]:=Water`ToString[a,Flag]<>"-"<>Water`ToString[b,Flag];
Water`ToString[a_+b_,Flag_:False]:=Water`ToString[a,Flag]<>"+"<>Water`ToString[b,Flag];
Water`ToString[a_ b_,Flag_:False]:=Water`ToString[a,Flag]<>"*"<>If[Head[b]===Plus,"(",""]<>Water`ToString[b,Flag]<>If[Head[b]===Plus,")",""];
Water`ToString[a_[b_],Flag_:False]:=Water`ToString[a,Flag]<>Water`ToString[b,Flag];
Water`ToString[Power[a_Symbol,2],Flag_:False]:=Water`ToString[a]<>"2";
Water`ToString[FourMomentum[Internal,i_Integer],Flag_:False]:="k"<>ToString[i];
Water`ToString[FourMomentum[External,i_Integer],Flag_:False]:="p"<>ToString[i];
Water`ToString[a_?NumericQ,Flag_:False]:=ToString[N[a]];
Water`ToString[a_String,Flag_:False]:=a;
Water`ToString[a_Symbol,Flag_:False]:=ToLowerCase[ToString[FullForm[a]]];


(* ::Subsubsubsection::Closed:: *)
(*C++ main program*)


Water`Write::usage="Water`Write is internally used to automatically write Water .cpp input files.";
Water`Write[FileHead_String,TopN_Integer,TopClass_Integer]:=
Module[
{
Dens=Denominators[Top[TopN]],
IntMoms,ExtMoms,Dots,
Scales,
FileStream,
DeclareSymbol,DeclareDenominator,AddDot,
DenominatorCounter=1
},
IntMoms=DeleteDuplicates[Cases[Dens,FourMomentum[Internal,_Integer],Infinity]];
ExtMoms=Sort[DeleteDuplicates[Cases[Dens,FourMomentum[External,_Integer],Infinity]]];
Dots=Dot(*SP*)@@@Subsets[Sort[ExtMoms],{2}];
Scales=DeleteDuplicates[Cases[{Denominators[Top[TopN]],SQ/@ExtMoms},Except[_SP|_FourMomentum|_Power|_Integer|_Times|_Plus|_List|Internal|External]|SP[FourMomentum[External,a_],FourMomentum[External,a_]],Infinity]];
If[Water`Verbose,Print["I found ",Length[ExtMoms]," external legs and ",Length[IntMoms]," loops."]];
If[Water`Verbose,Print["Namely, I think ",ExtMoms," are the external momenta and ",IntMoms," the loops'."]];
If[Water`Verbose,Print["The dot products I see are ",Dots]];
If[Water`Verbose,Print["List of scales: ",Scales]];
(*Open the file stream*)
FileStream=OpenWrite[StringJoin[Water`Path,FileHead,ToString[TopClass],".cpp"]];
(*Internal functions*)
DeclareSymbol[x_]:=WriteString[FileStream,"   symbol ",Water`ToString[x]," = sym(\"",Water`ToString[x],"\");\n"];
DeclareDenominator[Stuff_]:=WriteString[FileStream,"   denoms.push_back(Denominator(",Water`ToString[Stuff,True],",0,",DenominatorCounter++,"));\n"];
AddDot[k1_,k2_]:=WriteString[FileStream,"   IBP.AddDot(",Water`ToString[k1],",",Water`ToString[k2],",",Water`ToString[SP[k1,k2]],");\n"];
(*Write the head of the program*)
WriteString[FileStream,Water`Main`Head];
(*Declare names*)
WriteString[
FileStream,
StringJoin[
"   string intsym=\"",FileHead,ToString[TopClass],"\";\n",
"   string mytop=\"",FileHead,ToString[TopClass],"\";\n"
]
];
(*Declare symbols*)
DeclareSymbol/@IntMoms;
DeclareSymbol/@ExtMoms;
DeclareSymbol/@Dots;
DeclareSymbol[If[MatchQ[#,SP[__]],#,Power[#,2]]]&/@Scales;
(*Declare loop momenta*)
WriteString[FileStream,"   loopmom.push_back(",Water`ToString[#],");\n"]&/@IntMoms;
(*Declare denominators*)
DeclareDenominator/@Dens;
(*Set up IBPMaker and generate IBPs*)
WriteString[FileStream,"   IBPMaker IBP(loopmom);\n"];
AddDot[#1,#1]&/@ExtMoms;
AddDot[#[[1]],#[[2]]]&/@Dots;
WriteString[FileStream,"   IBP.GenerateIBPs(denoms);\n   IBP.PrintIBPs();\n"];
WriteString[FileStream,"   IBP.PrintDots();\n   IBP.MathematicaExport(path+\"Reductions/\"+mytop+\"_IBP.txt\");\n"];
(*Start Water*)
WriteString[FileStream,"   Water* water=new Water(&IBP,mytop);\n   water\[Rule]SetIntegralSymbol(intsym);\n"];
(*Set up Water parameters*)
WriteString[FileStream,StringJoin["   water->maxpos=",ToString[Water`MaxPos[[TopClass]]],";\n"]];
WriteString[FileStream,StringJoin["   water->maxneg=",ToString[Water`MaxNeg[[TopClass]]],";\n"]];
WriteString[FileStream,StringJoin["   water->SetMaxNumOfProps(",ToString[Water`MaxNumOfProps[[TopClass]]],");\n"]];
(*Write the tail of the program*)
WriteString[FileStream,Water`Main`Tail];
(*Close the file*)
Close[FileStream];
If[Water`Verbose,Print["Finished writing Water *.cpp file."]];
];


(* ::Subsubsection:: *)
(*Running Water reduction*)


(* ::Subsubsubsection::Closed:: *)
(*Run command*)


Water`Run[FileHead_String,i_Integer]:=
Run["source ~/.profile && cd "<>StringDrop[Water`Path,-1]<>" && make -j16 "<>FileHead<>ToString[i]<>".out && ./"<>FileHead<>ToString[i]<>".out 2>&1"];


(* ::Subsubsection:: *)
(*Reading Water output*)


(* ::Subsubsubsection::Closed:: *)
(*Read command*)


Water`Read[FileHead_String,TopN_Integer,TopClass_Integer]:=
Module[
{
Dens=Denominators[Top[TopN]],
ExtMoms,Dots,
Scales,
FileStream,
DeclareSymbol,DeclareDenominator,AddDot,
DenominatorCounter=1
},
ExtMoms=DeleteDuplicates[Cases[Dens,FourMomentum[External,_Integer],Infinity]];
Dots=SP@@@Subsets[ExtMoms,{2}];
Scales=DeleteDuplicates[Cases[Denominators[Top[TopN]],Except[_SP|_FourMomentum|_Power|_Integer|_Times|_Plus|Internal|External],Infinity]];
If[Water`Verbose,Print[Scales]];
(*Read Water output file*)
Get[Water`Path<>"../test/Reductions/"<>FileHead<>ToString[TopClass]<>".txt"];
RuleDelayed@@@Transpose[
{
ReplaceAll[
Transpose[List@@@DownValues[Evaluate[ToExpression[FileHead<>ToString[TopClass]]]]][[1]],
ToExpression[FileHead<>ToString[TopClass]][\[Nu]s__Integer]:>Apply[Integral[Top[TopN]],-{\[Nu]s}]
]/.HoldPattern->Identity,
ReplaceRepeated[
Transpose[List@@@DownValues[Evaluate[ToExpression[FileHead<>ToString[TopClass]]]]][[2]],
Flatten[
Join[
ToExpression[Water`ToString[#]]->#&/@Dots,
ToExpression[Water`ToString[Power[#,2]]]->Power[#,2]&/@Scales,
{
d->MyDim,
ToExpression[FileHead<>ToString[TopClass]][\[Nu]s__Integer]:>Apply[Integral[Top[TopN]],-{\[Nu]s}]
}
]
]
]
}
]
];


(* ::Subsubsubsection::Closed:: *)
(*Rotating reduced results*)


Water`Rotate[i_Integer->j_Integer][RedList_List]:=
Module[
{
Rot=TopologyRotation[i->j],
Perm
},
If[Water`Verbose,Print["My rotation is ",Rot]];
Perm=FindPermutation[
Denominators[Top[j]],
ReplaceAll[
Denominators[Top[i]]/.FourMomentum[Internal,k_Integer]:>FourMomentum[Internal,k]+FourMomentum[External,-k],
Rot
]//Expand
];
ReplaceAll[
RedList/.Rot,
Integral[Top[i]][\[Nu]s__Integer]:>Integral[Top[j]]@@Permute[{\[Nu]s},Perm]
]
];


(* ::Subsubsection:: *)
(*Automatic reduction*)


(* ::Subsubsubsection::Closed:: *)
(*Water command*)


Water[Expr_,AutoConf_:True,Verbose_:False]:=Module[
{
TopClasses=Map[
#[[1]]&,
Gather[
DeleteDuplicates[Cases[Expr,Top[_Integer],Infinity,Heads->True]],
TopologyRotation[#1[[1]]->#2[[1]]]=!={}&
],
{2}
],
L,
TmpSubs
},
L=Length[TopClasses];
If[Verbose,Print["The topology equivalence classes wrt rotations are ",TopClasses]];
Water`Verbose=Verbose;
If[Verbose,Print["Configuring Water..."]];
If[AutoConf,Water`AutoConf[Expr]];
If[Verbose,Print["Configuration done."]];
Water`Write["Foo",#[[1]][[1]],#[[2]]]&/@Transpose[{TopClasses,Range[L]}];
If[Verbose,Print["Starting  reduction run..."]];
Water`Run["Foo",#]&/@Range[L];
If[Verbose,Print["I think the reduction is over.\nNow reading results..."]];
TmpSubs=Water`Read["Foo",#[[1]][[1]],#[[2]]]&/@Transpose[{TopClasses,Range[L]}];
If[Verbose,Print["Finished reading."]];
Run["source ~/.profile && cd "<>StringDrop[Water`Path,-1](*<>" && rm Foo* && cd ../test/Reduction && rm Foo*"*)];
ClearAll[Evaluate[ToExpression["Foo"<>ToString[#[[1]]]]]]&/@TopClasses;
Water`Verbose=False;
Water`LastSubs=Flatten[
Table[Water`Rotate[TopClasses[[i]][[1]]->TopClasses[[i]][[j]]][TmpSubs[[i]]],{i,1,Length[TopClasses]},{j,1,Length[TopClasses[[i]]]}]
];
Expr/.Water`LastSubs
];


(* ::Subsubsubsection::Closed:: *)
(*ReadReduction command*)


ReadReduction[Expr_,MyHead_:"Foo",Verbose_:False]:=Module[
{
TopClasses=Map[
#[[1]]&,
Gather[
DeleteDuplicates[Cases[Expr,Top[_Integer],Infinity,Heads->True]],
TopologyRotation[#1[[1]]->#2[[1]]]=!={}&
],
{2}
],
L,
TmpSubs
},
L=Length[TopClasses];
If[Verbose,Print["The topology equivalence classes wrt rotations are ",TopClasses]];
Water`Verbose=Verbose;
TmpSubs=Water`Read[MyHead,#[[1]][[1]],#[[2]]]&/@Transpose[{TopClasses,Range[L]}];
If[Verbose,Print["Finished reading."]];
ClearAll[Evaluate[ToExpression[MyHead<>ToString[#[[1]]]]]]&/@TopClasses;
Water`LastSubs=Flatten[
Table[Water`Rotate[TopClasses[[i]][[1]]->TopClasses[[i]][[j]]][TmpSubs[[i]]],{i,1,Length[TopClasses]},{j,1,Length[TopClasses[[i]]]}]
];
Water`Verbose=False;
Expr/.Water`LastSubs
];


(* ::Subsection:: *)
(*Masters*)


(* ::Subsubsection::Closed:: *)
(*Recognizing masters*)


ToMasters["Bubbles",Verbose_:False]:=
Integral[MyTop_][\[Nu]s__Integer]:>
Module[
{MyDenoms=Pick[Denominators[MyTop],{\[Nu]s},1]},
If[Verbose,Print["Bubble ",MyDenoms," found."]];
Switch[
Length[Shift`LoopMomenta[MyDenoms]],
(*Zero-loop bubble does not exist*)
0,
Message["Something's wrong with the topology"],
(*One-loop bubble*)
1,
Module[
{
D1=Shift`ABCForm[MyDenoms[[1]],1],
D2=Shift`ABCForm[MyDenoms[[2]],1]
},
(*Massless bubble if both denominators are perfect squares*)
If[
Reduce[SQ[D1[[2]][[1]]]==4D1[[1]][[1]][[1]]D1[[3]]&&SQ[D2[[2]][[1]]]==4D2[[1]][[1]][[1]]D2[[3]]],
Bubble[SQ[D2[[2]][[1]]/(2D2[[1]][[1]][[1]])-D1[[2]][[1]]/(2D1[[1]][[1]][[1]])]][1,1],
Message["I don't know how to recognise massive bubbles."]
]
],
_,
Message["Two-loop bubble found."]
]
]/;Count[{\[Nu]s},1]==2&&Apply[And,#==1||#==0&/@{\[Nu]s}];


ToMasters["Boxes",Verbose_:False]:=
Integral[Top[i_Integer]][\[Nu]s__Integer]:>
Module[
{
MyRotation=TopologyRotation[
Rule[
\!\(\*
TagBox[
StyleBox[
RowBox[{"List", "[", 
RowBox[{
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}], ",", 
RowBox[{"Plus", "[", 
RowBox[{
RowBox[{"Times", "[", 
RowBox[{
RowBox[{"-", "2"}], ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"External", ",", "1"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"Plus", "[", 
RowBox[{
RowBox[{"Times", "[", 
RowBox[{"2", ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"External", ",", "2"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"Plus", "[", 
RowBox[{
RowBox[{"Times", "[", 
RowBox[{"2", ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"External", ",", "2"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"External", ",", "4"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"Times", "[", 
RowBox[{"2", ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"External", ",", "2"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"Times", "[", 
RowBox[{"2", ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"External", ",", "4"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}], ",", 
RowBox[{"SP", "[", 
RowBox[{
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}], ",", 
RowBox[{"FourMomentum", "[", 
RowBox[{"Internal", ",", "1"}], "]"}]}], "]"}]}], "]"}]}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\),
Pick[Denominators[Top[i]],{\[Nu]s},1]
]
]
},
Which[
(*Massless Box*)
MyRotation=!={},
BoxF[SQ[FourMomentum[External,1]+FourMomentum[External,2]],SQ[FourMomentum[External,2]+FourMomentum[External,4]],SQ[FourMomentum[External,1]+FourMomentum[External,2]+FourMomentum[External,4]]]/.MyRotation,
(*Failure*)
True,
Message["You should implement boxes with masses!!"]
]
]/;Count[{\[Nu]s},1]==4&&Apply[And,#==1||#==0&/@{\[Nu]s}];


(* ::Input:: *)
(*ToMasters[Stuff__String]:=Sequence@@@ToMasters/@{Stuff}/;Length[{Stuff}]!=1;*)


(* ::Subsubsection:: *)
(*Master integrals*)


(* ::Subsubsubsection::Closed:: *)
(*Preliminaries*)


(* ::Subsubsubsubsection::Closed:: *)
(*One loop prefactor*)


Toc\[CapitalGamma][Expr_]:=-((2 Gamma[-2 \[Epsilon]] Subscript[c,\[CapitalGamma]])/(\[Epsilon]^2 Gamma[-\[Epsilon]]^2 Gamma[\[Epsilon]])) ReplaceAll[Expr,Subscript[c,\[CapitalGamma]]->-((\[Epsilon]^2 Gamma[-\[Epsilon]]^2 Gamma[\[Epsilon]])/(2 Gamma[-2 \[Epsilon]]))];
Subc\[CapitalGamma]=Rule[Subscript[c,\[CapitalGamma]],Gamma[1+\[Epsilon]]Gamma[1-\[Epsilon]]^2/(Power[4\[Pi],-\[Epsilon]]Gamma[1-2 \[Epsilon]])];


ArgSimplify[Expr_]:=Expr//.{Gamma[x_]:>Gamma[Simplify[x]]}//.{Hypergeometric2F1[a_,b_,c_,x_]:>Simplify[Hypergeometric2F1[a,b,c,x]],Power[a_,b_]:>Power[Factor[a],Simplify[b]],Bubble[s_][n_,m_]:>Bubble[Simplify[s]][n,m],BoxF[a__]:>Simplify[BoxF[a]]}


QuickMasterFix[Expr_]:=Toc\[CapitalGamma][ArgSimplify[Expr/.{MyDim->4-2\[Epsilon]}]//.ToGammas[\[Epsilon],-\[Epsilon],-2\[Epsilon]]]


(* ::Subsubsubsection::Closed:: *)
(*Framework*)


ClearAll[DoMasters];
DoMasters["Bubbles"]={};
DoMasters["Triangles"]={};
DoMasters["Boxes"]={};
DoMasters[Stuff__String]:=Sequence@@@DoMasters/@{Stuff}/;Length[{Stuff}]!=1;
DoMasters[]:=DoMasters["Bubbles","Triangles","Boxes"];
DoMasters["All"]:=DoMasters[];


(* ::Subsubsubsection:: *)
(*Bubbles*)


(* ::Subsubsubsubsection::Closed:: *)
(*Auxiliary function \[CapitalPi]*)


(*Note: differs from conventional definition by a factor (-1)^(MyDim/2)*)
\[CapitalPi][\[Nu]2_,\[Nu]3_]:=(Gamma[\[Nu]2+\[Nu]3-MyDim/2]Gamma[MyDim/2-\[Nu]2]Gamma[MyDim/2-\[Nu]3])/(Gamma[\[Nu]2]Gamma[\[Nu]3]Gamma[MyDim-\[Nu]2-\[Nu]3]);


(* ::Subsubsubsubsection::Closed:: *)
(*Bubble master*)


AppendTo[
DoMasters["Bubbles"],
Bubble[m2_][\[Nu]1_,\[Nu]2_]:>\[CapitalPi][\[Nu]1,\[Nu]2](-1)^(\[Nu]1+\[Nu]2) (-m2)^(MyDim/2-\[Nu]1-\[Nu]2)
];


(* ::Subsubsubsection:: *)
(*Triangles*)


(* ::Subsubsubsubsection::Closed:: *)
(*Triangle with one massive external leg*)


AppendTo[
DoMasters["Triangles"],
Triangle[m2_][\[Nu]1_Integer,\[Nu]2_Integer,\[Nu]3_Integer]:>
Module[
{d=MyDim,\[CapitalGamma]=Gamma},
(-1)^(\[Nu]1+\[Nu]2+\[Nu]3) (\[CapitalGamma][\[Nu]1+\[Nu]2+\[Nu]3-d/2]\[CapitalGamma][d/2-\[Nu]2-\[Nu]3]\[CapitalGamma][d/2-\[Nu]1-\[Nu]2])/(\[CapitalGamma][\[Nu]1]\[CapitalGamma][\[Nu]3]\[CapitalGamma][d-\[Nu]1-\[Nu]2-\[Nu]3]) (-m2)^(d/2-\[Nu]1-\[Nu]2-\[Nu]3)
]
];


(* ::Subsubsubsubsection::Closed:: *)
(*Triangle with two massive external legs*)


AppendTo[
DoMasters["Triangles"],
Triangle[m12_,m22_][\[Nu]1_Integer,\[Nu]2_Integer,\[Nu]3_Integer]:>
Module[
{d=MyDim,\[CapitalGamma]=Gamma},
(-1)^(\[Nu]1+\[Nu]2+\[Nu]3) (\[CapitalGamma][\[Nu]1+\[Nu]2+\[Nu]3-d/2]\[CapitalGamma][d/2-\[Nu]2]\[CapitalGamma][d/2-\[Nu]1-\[Nu]3])/(\[CapitalGamma][\[Nu]2]\[CapitalGamma][\[Nu]1+\[Nu]3]\[CapitalGamma][d-\[Nu]1-\[Nu]2-\[Nu]3]) (-m22)^(d/2-\[Nu]1-\[Nu]2-\[Nu]3) F21[\[Nu]1+\[Nu]2+\[Nu]3-d/2,\[Nu]1,\[Nu]1+\[Nu]3,1-m12/m22]
]
];


(* ::Subsubsubsubsection::Closed:: *)
(*Dumb Xcheck with bubble*)


Triangle[s][1,0,1]-Bubble[s][1,1]/.DoMasters[]


(* ::Subsubsubsection:: *)
(*Boxes*)


(* ::Subsubsubsubsection::Closed:: *)
(*Box with one massive external leg*)


AppendTo[
DoMasters["Boxes"],
BoxF[s_,t_,M2_]:>Module[{u=M2-s-t},
2/\[Epsilon]^2 Subscript[c, \[CapitalGamma]] 1/(s t) (
(-t)^-\[Epsilon] Hypergeometric2F1[1,-\[Epsilon],1-\[Epsilon],-(u/s)]+
(-s)^-\[Epsilon] Hypergeometric2F1[1,-\[Epsilon],1-\[Epsilon],-(u/t)]-
(-M2)^-\[Epsilon] Hypergeometric2F1[1,-\[Epsilon],1-\[Epsilon],-((M2 u)/(s t))]
)
]
];


(*This was here but WRONG*)
(*Symmetry only in first two arguments...??!?!?! change to BoxF[a,b][c]????*)
(*SetAttributes[BoxF,Orderless]*)


(*To be eliminated after cross-check of the expression above... bugged for some reason*)
(*BoxF[s_,t_,M2_]\[RuleDelayed]Module[{u=M2-s-t},2/\[Epsilon]^2Subscript[c, \[CapitalGamma]]1/(st)((-t)^-\[Epsilon]Hypergeometric2F1[1,-\[Epsilon],1-\[Epsilon],-(u/s)]+(-s)^-\[Epsilon]Hypergeometric2F1[1,-\[Epsilon],1-\[Epsilon],-(u/t)]-(-M2)^-\[Epsilon]Hypergeometric2F1[1,-\[Epsilon],1-\[Epsilon],-((M2u)/(st))])]*)


(* ::Section:: *)
(*Strategy of regions*)


(* ::Subsection:: *)
(*Finding regions*)


(* ::Subsubsection::Closed:: *)
(*Collinear regions*)


(*Note: Amplitudes must be passed to FindCollinearRegion when they still have PropagatorDenominators!!!*)
FindCollinearRegion[Momenta_,Verbose_:False][Amp_]:=Module[
{PropArgs,PropMoms,PropMomPairs,MomDiffs},
PropArgs=Map[
Cases[#,a_PropagatorDenominator,Infinity]&,
Amp
];
PropMoms=Map[
#[[1]]&,
PropArgs,
{2}
];
If[Verbose,Print[PropMoms]];
PropMomPairs=Subsets[#,{2}]&/@PropMoms;
If[Verbose,Print[PropMomPairs]];
MomDiffs=Apply[Plus[#1,-#2]&,PropMomPairs,{2}];
If[Verbose,Print[MomDiffs]];
If[Verbose,Print[Map[FreeQ[CoefficientList[#,Momenta],FourMomentum]&,MomDiffs,{2}]]];
Pick[PropMomPairs,Map[FreeQ[CoefficientList[#,Momenta],FourMomentum]&,MomDiffs,{2}]]
];


SimplifyRuleList[RuleList_List,Verbose_:False]:=
Module[
{PrevList={},NewList=RuleList,i=0},
If[Verbose,Print["Called SymplifyRuleList[",RuleList,"]\nStarting PrevList is ",PrevList,"\nStarting NewList is  ",NewList]];
While[
PrevList=!=NewList,
PrevList=NewList;
Module[
{FirstStep=#/.Complement[NewList,{#}]&/@NewList},
NewList=Map[Simplify,FirstStep,{2}]/.HoldPattern[Rule[a_?NumericQ b_,0]]:>Rule[b,0]
];
If[Verbose,Print["Step N. ",++i,": ",PrevList->NewList]];
];
NewList
]


ReplaceExpansion[Amp_,Rules_,DoNumerator_:False,Verbose_:False]:=
Module[
{
MyD=Denominator[Amp],
MyN=Numerator[Amp],
NewD,NewN
},
If[Head[MyD]===Times,MyD=List@@MyD,MyD={MyD}];
If[Head[MyN]===Times,MyN=List@@MyN,MyN={MyN}];
If[Verbose,Print["I will try to apply the rules ",Rules," to ",MyD]];
NewD=Map[If[ReplaceAll[#1,Rules]=!=0,#1//.Rules,#1]&,MyD];
NewN=Map[If[ReplaceAll[#1,Rules]=!=0,#1//.Rules,#1]&,MyN];
If[Verbose,Print["The denominators read ",ReplaceRepeated[MyD,Rules]," and I shall replace them with ",NewD]];
If[DoNumerator,Times@@NewN,Numerator[Amp]]/(Times@@NewD)
];
